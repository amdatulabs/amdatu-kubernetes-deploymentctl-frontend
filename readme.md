Introduction
===
Amdatu Kubernetes Deploymentctl provides basically a UI for the Amdatu Kubernetes Deployer, but it has some additional features.

This repository contains the frontend part, see the Amdatu Kubernetes Deploymentctl Backend repository for the backend
and more detailed information.

Getting involved
===
[Bug reports and feature requests](https://amdatu.atlassian.net/projects/AKDCTL) and of course pull requests are greatly appreciated!
The project is built on [Bamboo](https://amdatu.atlassian.net/builds/browse/AKDCTLF-MAIN/latestSuccessful).
The build produces a transpiled and bundled version of the frontend and pushes a Docker image.

Versioning
===

The Docker images are tagged using a alpha/beta/prod schema.
The continuous build pushes `amdatu/deploymentctl-backend:alpha`.
When a version is stable, we promote it to `beta`.
The image is not rebuild, but a tag is set in git specifying the version number (e.g. 1.x.x).
For Docker we set multiple tags:

* beta
* 1.x.x

This way you can both reference the latest beta version, or a specific tagged version.
From beta we promote images to `production`.
The image is not rebuild, but new tags are added:

* production
* latest

This way you can reference the latest stable production version.

The list of tags can be found on [Docker Hub](https://hub.docker.com/r/amdatu/amdatu-kubernetes-deploymentctl-frontend/tags/).