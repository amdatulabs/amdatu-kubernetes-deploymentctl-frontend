/**
@license Licensed under Apache License v2. See LICENSE for more information.
*/
import {Host} from '../../../src/app/util/host'

describe('Http Hosts', () => {
    it('localhost 3000 to 8080', () => expect(Host['getBackendUrlInternal']('http:', 'localhost:3000')).toBe('http://localhost:8080'));
    it('localhost 8080', () => expect(Host['getBackendUrlInternal']('http:', 'localhost:8080')).toBe('http://localhost:8080'));
    it('test.amdatu.com', () => expect(Host['getBackendUrlInternal']('http:', 'test.amdatu.com')).toBe('http://test.amdatu.com'));
});

describe('Websocket Hosts', () => {
    it('localhost 3000 to 8080', () => expect(Host['getBackendWebsocketUrlInternal']('http:', 'localhost:3000', true)).toBe('ws://localhost:8080/deploymentctl/websocket/'));
    it('localhost 8080', () => expect(Host['getBackendWebsocketUrlInternal']('http:', 'localhost:8080', true)).toBe('ws://localhost:8080/deploymentctl/websocket/'));
    it('rti-production-deployer.amdatu.com to rti-production-deployer.amdatu.com',
        () => expect(Host['getBackendWebsocketUrlInternal']('https:', 'rti-production-deployer.amdatu.com', true))
            .toBe('wss://rti-production-deployer.amdatu.com/deploymentctl/websocket/'));
    it('test.cloudrti.com to test.cloudrti.com',
        () => expect(Host['getBackendWebsocketUrlInternal']('https:', 'test.cloudrti.com', true))
            .toBe('wss://test.cloudrti.com/deploymentctl/websocket/'));
});
