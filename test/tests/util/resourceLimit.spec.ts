/**
@license Licensed under Apache License v2. See LICENSE for more information.
*/
import {ResourceDefinition} from '../../../src/app/util/resourceLimit';

describe('Parse Resource Limits', () => {

    it('100m', () => {
        expect(ResourceDefinition.createFromString('100m')).toEqual(new ResourceDefinition(100, "m"));
    });
    it('10.23Gi', () => {
        expect(ResourceDefinition.createFromString('10.23Gi')).toEqual(new ResourceDefinition(10.23, "Gi"));
    });
    it('.12K', () => {
        expect(ResourceDefinition.createFromString('.12K')).toEqual(new ResourceDefinition(.12, "K"));
    });
    it('42', () => {
        expect(ResourceDefinition.createFromString('42')).toEqual(new ResourceDefinition(42, ""));
    });
    it('45.12', () => {
        expect(ResourceDefinition.createFromString('45.12')).toEqual(new ResourceDefinition(45.12, ""));
    });

});