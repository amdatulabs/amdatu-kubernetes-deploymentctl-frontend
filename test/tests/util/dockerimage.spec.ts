/**
@license Licensed under Apache License v2. See LICENSE for more information.
*/
import {DockerImage} from '../../../src/app/util/dockerimage';

describe('Parse Docker Image', () => {

    it('image:tag', () => {
        expect(DockerImage.createFromString('image:tag')).toEqual(new DockerImage('image', 'tag'));
    });

    it('test-repo.repo.com:5000/user/image:tag', () => {
        expect(DockerImage.createFromString('test-repo.repo.com:5000/user/image:tag')).toEqual(new DockerImage('test-repo.repo.com:5000/user/image', 'tag'));
    })

});
