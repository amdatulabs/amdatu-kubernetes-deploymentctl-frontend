/**
@license Licensed under Apache License v2. See LICENSE for more information.
*/
System.config({
    transpiler: 'typescript',
    //map tells the System loader where to look for things
    map: {
        app: "../src",
        tests: "tests"
    },
    //packages defines our app package
    packages: {
        app: {
            defaultExtension: 'ts'
        },
        tests: {
            defaultExtension: 'ts'
        }
    }
});
