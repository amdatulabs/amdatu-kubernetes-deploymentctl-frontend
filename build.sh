#!/usr/bin/env bash

# fail immediately
set -e
set -o pipefail

IMAGE=amdatu/amdatu-kubernetes-deploymentctl-frontend:latest
docker build -t "$IMAGE" .
docker login --username="$DOCKER_USER" --password="$DOCKER_PASSWORD"
docker push "$IMAGE"
