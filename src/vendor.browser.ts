/**
 @license Licensed under Apache License v2. See LICENSE for more information.
 */
// For vendors for example jQuery, Lodash, angular2-jwt just import them here unless you plan on
// chunking vendors files for async loading. You would need to import the async loaded vendors
// at the entry point of the async loaded file. Also see custom-typings.d.ts as you also need to
// run `typings install x` where `x` is your module

// Angular 2
import '@angular/platform-browser';
import '@angular/platform-browser-dynamic';
import '@angular/core';
import '@angular/common';
import '@angular/forms';
import '@angular/http';
import '@angular/router';

// RxJS
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/onErrorResumeNext';
import 'rxjs/add/operator/catch';

// 3rd party
import 'jquery';
import 'bootstrap-loader';
import 'angular2-jwt';

if ('production' === ENV) {
  // Production


} else {
  // Development

}
