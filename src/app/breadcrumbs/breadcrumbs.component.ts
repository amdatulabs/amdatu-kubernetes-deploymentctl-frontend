/**
 @license Licensed under Apache License v2. See LICENSE for more information.
 */
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';

@Component ({
  selector: 'breadcrumbs',
  template: require('./views/breadcrumbs.component.html')
})

export class BreadcrumbsComponent implements OnInit {

  segments: Segment[] = [];

  constructor(private router: Router, private activatedRoute: ActivatedRoute){

  }

  ngOnInit() {
    this.router.events.subscribe( event => {
      if (event instanceof NavigationEnd) {
        this.segments = [];

        let params = this.router.routerState.root.firstChild.snapshot.params;

        if(event.url.localeCompare('/') === 0) {
           this.addApplications(params);
        } else if(event.url.startsWith('/listApplications')) {
          this.addApplications(params);
        } else if(event.url.startsWith('/dockerHub')) {
          this.addDockerHub(params);
        } else if (event.url.startsWith('/listSecrets')) {
          this.addSecrets(params);
        } else if (event.url.startsWith('/descriptor')) {
          this.addApplications(params);
          if(params['deploymentId']) {
            this.addDeployment(params);
          }
          this.addDescriptor(params);
        } else if (event.url.startsWith('/deployment')) {
          this.addApplications(params);
          this.addDeployment(params);
        }
      }
    });
  }

  private addApplications(params) {
    this.segments.push({label: 'Applications', routerLink: '/listApplications', routerParams: params});
  }

  private addSecrets(params) {
    this.segments.push({label: 'Secrets', routerLink: '/listSecrets', routerParams: {}});
  }

  private addDockerHub(params) {
    this.segments.push({label: 'Docker Hub', routerLink: '/dockerHub', routerParams: {}});
  }

  private addDescriptor(params) {
    if(params['id'] || params['deploymentId']) {
      this.segments.push({label: 'Descriptor: ' + params['namespace'] + '/' + params['appName'], routerLink: '/descriptor', routerParams: params});
    } else {
      this.segments.push({label: 'New deployment descriptor', routerLink: '/descriptor', routerParams: params});
    }
  }

  private addDeployment(params) {
    if(params['deploymentId']) {
      this.segments.push({label: 'Deployment: ' + params['namespace'] + '/' + params['appName'], routerLink: '/deployment', routerParams: {namespace: params['namespace'], appName: params['appName'], id: params['deploymentId']}});
    } else {
      this.segments.push({label: 'Deployment: ' + params['namespace'] + '/' + params['appName'], routerLink: '/deployment', routerParams: params});
    }

  }

}

class Segment {
  constructor(public label: string, public routerLink: string, public routerParams: any){}
}
