/**
 @license Licensed under Apache License v2. See LICENSE for more information.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class KeycloakService {

  private keycloak: any;

  constructor() {
    this.keycloak = window['_keycloak'];
  }

  public logout() {
    this.keycloak.logout();
  }

  public getUserInfo() {
    return this.keycloak.loadUserInfo();
  }

  public getToken(): string {
    return this.keycloak.token;
  }
}
