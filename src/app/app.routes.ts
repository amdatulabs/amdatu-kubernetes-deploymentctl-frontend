/**
 @license Licensed under Apache License v2. See LICENSE for more information.
 */
import { Routes } from '@angular/router';
import { DockerHubComponent } from './dockerHub/dockerHub.component';
import { ApplicationListComponent } from './deployment/application-list.component';
import { DescriptorDetailsComponent } from './deployment/descriptor-details.component';
import { DeploymentDetailsComponent } from './deployment/deployment-details.component';
import { SecretListComponent } from './secrets/secret-list.component';

export const ROUTES: Routes = [

  {path: '', component: ApplicationListComponent},
  {path: 'dockerHub', component: DockerHubComponent},
  {path: 'listApplications', component: ApplicationListComponent},
  {path: 'descriptor', component: DescriptorDetailsComponent},
  {path: 'deployment', component: DeploymentDetailsComponent},
  {path: 'listSecrets', component: SecretListComponent}

  // { path: '**', component: NoContent },
];
