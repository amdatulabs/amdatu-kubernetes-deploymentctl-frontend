/**
 @license Licensed under Apache License v2. See LICENSE for more information.
 */
import { Component, OnInit, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UUID } from 'angular2-uuid';
import { Logger } from '../util/logger';
import {
  Deployment,
  DeploymentType,
  PodSpec,
  Container,
  ImagePullSecret,
  ImagePullPolicy,
  DeploymentStatus,
  WebHook,
  HealthCheckType, HttpHeader, Descriptor, Volume, EmptyDir
} from './models/deployment';
import { DeploymentService } from './services/deployment.service';
import { PullSecretService } from './services/pull-secret.service';
import { DockerHubService } from '../dockerHub/services/dockerHub.service';
import { Observable } from 'rxjs';
import { Host } from '../util/host';

@Component({
  selector: 'descriptor-details',
  template: require('./views/descriptor-details.component.html')
})

export class DescriptorDetailsComponent implements OnInit {

  public static readonly VOLUME_NAME='deploymentctl-volume';

  private refreshImagesNotifier = new EventEmitter(true); // asynchronous
  private refreshVolumeNotifier = new EventEmitter(true); // asynchronous

  private descriptor: Descriptor;
  private descriptorBackup: Descriptor;

  private isLoggedIn = false;
  private isDockerLoggedIn = false;
  private isNew: boolean = true;
  private fromDeployment = false;

  private secrets: ImagePullSecret[];
  private selectedSecret: ImagePullSecret;
  private namespaces: string[] = [];
  private exposeToFrontend: boolean = false;
  private createVolume: boolean = false;

  private modaldialogContent = '';
  private modaldialogTitle = '';
  private modaldialogEditable = false;
  private modaldialogValid = true;
  private modaldialogAlert = '';

  private form: FormGroup;
  private dialogForm: FormGroup;

  constructor(private _deploymentService: DeploymentService,
              private _pullSecretService: PullSecretService,
              private _logger: Logger,
              private _dockerHubService: DockerHubService,
              private _fb: FormBuilder,
              private _router: Router,
              private _route: ActivatedRoute) {
    this.form = this._fb.group({
      namespace: [],
      combinedName: this._fb.group({
        name: [],
        version: []
      }),
      replicas: [],
      pullsecret: [],
      createVolume: [],
      expose: this._fb.group({
        exposeToFrontend: [],
        exposeUrl: []
      }),
      redirectWww: [],
      useCompression: [],
      useStickySessions: []
    });

    let _me = this;
    this.dialogForm = this._fb.group({
      modaldialog: [, (control: FormControl): {[key: string]: any} => {
        if (_me.modaldialogValid) {
          return null;
        } else {
          return {['validationerror']: true};
        }
      }]
    });
  }

  ngOnInit() {

    this.isDockerLoggedIn = this._dockerHubService.dockerLoggedIn;
    this.getNamespaces();

    this._route
      .params
      .subscribe(params => {

        let namespace = params['namespace'];
        let appName = params['appName'];
        let id = params['id'];
        let deploymentId = params['deploymentId'];

        if (id) {
          // get existing descriptor
          this._deploymentService.getDescriptor(namespace, id).subscribe(
            descriptor => this.initExistingDescriptor(descriptor),
            err => this._logger.error('Error getting descriptor', err),
            () => {}
          );

        } else if (deploymentId) {
          // get descriptor from deployment
          this._deploymentService.getDeployment(namespace, deploymentId, false).subscribe(
            deployment => this.initExistingDescriptor(deployment.descriptor),
            err => this._logger.error('Error getting deployment', err),
            () => {
              this.fromDeployment = true;
            }
          );

        } else {
          // init new empty deployment
          this.descriptor = <Descriptor>{};
          this.descriptor.webhooks = [];
          this.descriptor.additionHttpHeaders = [];
          this.descriptor.useHealthCheck = true;
          this.descriptor.healthCheckPath = 'health';
          this.descriptor.healthCheckPort = 9999;
          this.descriptor.healthCheckType = HealthCheckType.PROBE.toString();
          this.descriptor.ignoreHealthCheck = false;
          this.descriptor.podspec = <PodSpec>{};
          this.descriptor.replicas = 1;
          this.descriptor.podspec.containers = [this.getEmptyContainer()];
          this.descriptor.podspec.imagePullSecrets = [];
          if (this.namespaces.length === 1) {
            this.descriptor.namespace = this.namespaces[0];
            this.onNamespaceSelected(true);
          } else if (namespace) {
            this.descriptor.namespace = namespace;
            this.onNamespaceSelected(true);
            if (appName) {
              this.descriptor.appName = appName;
            }
          }
          this.descriptor.deploymentType = DeploymentType.BLUEGREEN.toString();
          this.descriptor.newVersion = '#';
          this.descriptor.useCompression = true;
          this.descriptor.useStickySessions = false;

        }

      });

    this.dialogForm.get('modaldialog').valueChanges
      .filter(_ => this.modaldialogEditable)
      .debounceTime(500)
      .distinctUntilChanged()
      .flatMap(s => this._deploymentService.validate(<string>s))
      .subscribe(
        message => {
          if (message.startsWith('Error')) {
            this.modaldialogValid = false;
            this.modaldialogAlert = message;
            this._logger.log('validation error: ' + message);
          } else {
            this.modaldialogValid = true;
            this.modaldialogAlert = '';
            this._logger.log('validation ok');
          }
          this.dialogForm.get('modaldialog').updateValueAndValidity();
        },
        err => this._logger.error('Error handling validation response: ', err),
        () => {}
      );

  }

  private initExistingDescriptor(descriptor: Descriptor) {
    this.descriptor = descriptor;
    this.isNew = false;
    if (this.descriptor.namespace) {
      this.onNamespaceSelected(false);
    }
    if (this.descriptor.frontend) {
      this.exposeToFrontend = true;
    }
    if (!this.descriptor.webhooks) {
      this.descriptor.webhooks = [];
    }
    if (!this.descriptor.additionHttpHeaders) {
      this.descriptor.additionHttpHeaders = [];
    }
    if (!this.descriptor.healthCheckPath) {
      this.descriptor.healthCheckPath = 'health';
    }
    if (!this.descriptor.healthCheckPort) {
      this.descriptor.healthCheckPort = 9999;
    }
    if (!this.descriptor.healthCheckType) {
      this.descriptor.healthCheckType = HealthCheckType.PROBE.toString();
    }
    if (this.descriptor.podspec.volumes) {
      let volume = this.descriptor.podspec.volumes.find(v => v.name === DescriptorDetailsComponent.VOLUME_NAME);
      if (volume) {
        this.createVolume = true;
      }
    }

  }

  private getNamespaces() {
    this.namespaces = [];
    this._dockerHubService.getNamespaces().subscribe(
      namespace => this.namespaces.push(namespace),
      err => this._logger.error('Error getting namespaces', err),
      () => {
        this._logger.log('getting namespaces success');
        if (this.descriptor && this.namespaces.length === 1) {
          this.descriptor.namespace = this.namespaces[0];
        }
      }
    );
  }

  private addContainer() {
    this._logger.log('addContainer');
    this.descriptor.podspec.containers.push(this.getEmptyContainer());
  }

  private getEmptyContainer(): Container {
    let container = <Container>{};
    container.ports = [];
    container.env = [];
    container.imagePullPolicy = ImagePullPolicy.ALWAYS.toString();
    return container;
  }

  private onContainerDeleted(container: Container) {
    let containers = this.descriptor.podspec.containers;
    if (containers.length > 1) {
      containers.splice(containers.indexOf(container), 1);
    }
    this.form.updateValueAndValidity({onlySelf: false, emitEvent: true});
  }

  private onNamespaceSelected(autoSelectSecret: boolean) {

    if (!this.descriptor.namespace) {
      return;
    }

    this._pullSecretService.getPullSecrets(this.descriptor.namespace).subscribe(
      secrets => this.secrets = secrets,
      error => this._logger.log('error getting secrets: ' + error.toString()),
      () => {
        if (this.descriptor.podspec.imagePullSecrets
          && this.descriptor.podspec.imagePullSecrets.length > 0) {
          this.selectedSecret = this.secrets.find(
            secret => secret.name === this.descriptor.podspec.imagePullSecrets[0].name);
        } else if (autoSelectSecret && this.secrets.length === 1) {
          this.selectedSecret = this.secrets[0];
          this.onSecretSelected();
        }
      }
    );
  }

  private onSecretSelected() {

    if (!this.selectedSecret || !this.selectedSecret.name) {
      return;
    }

    this._logger.log('onSecretSelected: ' + this.selectedSecret.name);

    let pullSecrets = this.descriptor.podspec.imagePullSecrets;
    if (!pullSecrets) {
      pullSecrets = [];
      this.descriptor.podspec.imagePullSecrets = pullSecrets;
    }

    if (!this.selectedSecret) {
      pullSecrets.splice(0, pullSecrets.length);
      return;
    }

    if (pullSecrets.length === 0) {
      pullSecrets.push(this.selectedSecret);
    } else {
      pullSecrets[0] = this.selectedSecret;
    }
  }

  private onFrontendChanged() {
    if (!this.exposeToFrontend) {
      this.descriptor.frontend = '';
    }
  }

  private onCreateVolumeChanged() {
    this.descriptor.podspec.volumes = [];
    if (this.createVolume) {
      let volume = <Volume>{name: DescriptorDetailsComponent.VOLUME_NAME, emptyDir: <EmptyDir>{}};
      this.descriptor.podspec.volumes.push(volume);
    }
    this.refreshVolumeNotifier.emit(this.createVolume);
  }

  private addWebhook() {
    this._logger.log('addWebhook');
    this.descriptor.webhooks.push(<WebHook>{key: UUID.UUID()});
  }

  private onWebhookDeleted(webhook: WebHook) {
    this.descriptor.webhooks.splice(this.descriptor.webhooks.indexOf(webhook), 1);
    this.form.updateValueAndValidity({onlySelf: false, emitEvent: true});
  }

  private addHeader() {
    this._logger.log('addHeader');
    this.descriptor.additionHttpHeaders.push(<HttpHeader>{});
  }

  private onHeaderDeleted(header: HttpHeader) {
    this.descriptor.additionHttpHeaders.splice(this.descriptor.additionHttpHeaders.indexOf(header), 1);
    this.form.updateValueAndValidity({onlySelf: false, emitEvent: true});
  }

  private onSave() {
    if (!this.form.valid) {
      this._logger.warn('Invalid descriptor, didn\'t save');
      return;
    }
    this.save().subscribe(
      id => {
        this._logger.info('Descriptor saved');
        if (this.isNew) {
          this._router.navigate(['/descriptor', { namespace: this.descriptor.namespace, appName: this.descriptor.appName, /**/id: id }]);
        }
      },
      err => this._logger.error('Error saving descriptor: ', err)
    );
  }

  private onDelete() {
    if (!this.descriptor.id) {
      this._logger.warn('Can not delete unsaved descriptor!');
      return;
    }
    this._deploymentService.deleteDescriptor(this.descriptor)
      .subscribe(
        _ => {
          this._logger.info('Descripor deleted');
          this._router.navigate(['/listApplications']);
        },
        err => this._logger.error('Error saving descriptor: ', err)
      );
  }

  private onDeploy() {
    if (!this.form.valid) {
      this._logger.warn('Invalid descriptor, didn\'t save and deploy!');
      return;
    }
    this.save().subscribe(
      descriptorId => {
        this._deploymentService.deploy(this.descriptor.namespace, descriptorId)
          .delay(200)
          .subscribe(
            deploymentId => {
              this._logger.info('Deployment started');
              this._router.navigate(
                ['/deployment', { namespace: this.descriptor.namespace, appName: this.descriptor.appName, id: deploymentId }]
              )
            },
            err => {
              this._logger.error('Error starting deployment: ', err);
              if (this.isNew) {
                this._router.navigate(['/descriptor', { namespace: this.descriptor.namespace, appName: this.descriptor.appName, id: descriptorId }]);
              }
            }
          );
      },
      err => {
        this._logger.error('Error saving descriptor: ', err);
      }

    );

  }

  private save(): Observable<string> {
    if (this.isNew) {
      return this._deploymentService.createDescriptor(this.descriptor);
    } else {
      return this._deploymentService.updateDescriptor(this.descriptor)
        .map(_ => this.descriptor.id);
    }
  }

  private onShow() {
    this.modaldialogEditable = !this.fromDeployment;
    this.modaldialogContent = JSON.stringify(this.descriptor, null, 2);
    this.modaldialogTitle = 'Deployment Descriptor';
    this.descriptorBackup = this.descriptor;
  }

  private onCancelDescriptor() {
    this.descriptor = this.descriptorBackup;
    this.modaldialogValid = true;
    this.modaldialogAlert = '';
    this._logger.info("Changes rolled back")
  }

  private onApplyDescriptor() {
    // first remove all container and webhook form controls,
    // we can't find the matching one, and will always add new ones..
    for (let key in this.form.controls) {
      if (this.form.controls[key].get('containername')) {
        this._logger.log('removing old container control with name ' + key);
        this.form.removeControl(key);
        continue;
      }
      if (this.form.controls[key].get('webhookkey')) {
        this._logger.log('removing old webhook control with name ' + key);
        this.form.removeControl(key);
      }
    }
    this.descriptor = <Descriptor>JSON.parse(this.modaldialogContent);
    this.modaldialogValid = true;
    this.modaldialogAlert = '';
    if (this.descriptor.frontend) {
      this.exposeToFrontend = true;
    }
    this.form.updateValueAndValidity();
    this._logger.info("Changes applied")
  }

  private refreshNamespaces() {
    this.getNamespaces();
  }

  private refreshImages() {
    this._dockerHubService.fetchRepositories().subscribe(
      _ => {
      },
      _ => {
      },
      () => this.notifyContainers()
    );
  }

  private notifyContainers() {
    this._logger.log('notifying containers about refreshed images');
    this.refreshImagesNotifier.emit('');
  }

  private isInDevEnv(): boolean {
    return Host.isInDevEnv();
  }
}

