/**
 @license Licensed under Apache License v2. See LICENSE for more information.
 */
import { Injectable } from '@angular/core';
import { AuthHttp, JwtHelper } from 'angular2-jwt';
import { Observable } from 'rxjs/Rx';
import { Logger } from '../../util/logger';
import { Host } from '../../util/host';
import { Deployment, Descriptor, HealthData } from '../models/deployment';
import { UUID } from 'angular2-uuid';
import { Response } from '@angular/http';
import { EventsService } from '../../util/events.service';

@Injectable()
export class DeploymentService {

  private host = Host.getBackendUrl();

  private deployments: Map<string, Map<string, Deployment[]>> = new Map();
  private descriptors: Map<string, Map<string, Descriptor[]>> = new Map();

  private eb: any;
  private jwtHelper: JwtHelper;

  private readonly ALL_APPS = '_all_apps_';

  constructor(private _authHttp: AuthHttp,
              private _eventsService: EventsService,
              private _logger: Logger) {

    this.jwtHelper = new JwtHelper();

  }

  getNamespaces(): Observable<string> {
    let decoded = this.jwtHelper.decodeToken(window['_keycloak'].token);
    return <Observable<string>>Observable.from(decoded['realm_access']['roles'])
      .filter(ns => ns !== 'admin');
  }

  createDescriptor(descriptor: Descriptor): Observable<string> {
    if (descriptor.id) {
      return Observable.throw('Id must not be set');
    }
    return this._authHttp.post(
        this.host + '/descriptors?namespace=' + descriptor.namespace,
        descriptor
      )
      .do(res => {
        this.getDescriptors(descriptor.namespace, true).subscribe();
        return res;
      })
      .map(res => this.getIdFromResponse(res));
  }

  updateDescriptor(descriptor: Descriptor): Observable<string> {
    if (!descriptor.id) {
      return Observable.throw('Id must be set');
    }
    return this._authHttp.put(
        this.host + '/descriptors/' + descriptor.id + '?namespace=' + descriptor.namespace,
        descriptor
      )
      .do(res => {
        this.getDescriptors(descriptor.namespace, true).subscribe();
        return res;
      })
      .map(res => res.statusText);
  }

  deleteDescriptor(descriptor: Descriptor): Observable<string> {
    if (!descriptor.id) {
      return Observable.throw('Id must be set');
    }
    return this._authHttp.delete(
      this.host + '/descriptors/' + descriptor.id + '?namespace=' + descriptor.namespace
      )
      .do(res => {
        this.getDescriptors(descriptor.namespace, true).subscribe();
        return res;
      })
      .map(res => res.statusText);
  }

  getDescriptors(namespace: string, reload: boolean): Observable<Map<string, Descriptor[]>> {

    if (!reload && this.descriptors.has(namespace)) {
      return Observable.of(this.descriptors.get(namespace));
    }

    // cache descriptors
    let apps = new Map<string, Descriptor[]>();
    this.descriptors.set(namespace, apps);

    return this._authHttp.get(
      this.host + '/descriptors?namespace=' + namespace
    )
      .map(res => res.json())
      .flatMap(d => {
        let descriptors = <Descriptor[]>d;
        for (let descriptor of descriptors) {
          if (!apps.has(descriptor.appName)) {
            apps.set(descriptor.appName, []);
          }
          apps.get(descriptor.appName).push(descriptor);
        }
        return Observable.of(apps);
      })
      .catch(err => {
        if (err.toString().indexOf('404') >= 0) {
          // no descriptor for namespace, that's ok
        } else {
          this._logger.error('Error getting descriptors for namespace ' + namespace, err);
        }
        return Observable.of(apps);
      });
  }

  getDescriptorsWithAppname(namespace: string, appName: string, reload: boolean): Observable<Map<string, Descriptor[]>> {
    if (!reload && this.descriptors.has(namespace)) {
      return Observable.of(this.descriptors.get(namespace));
    }

    // cache descriptors
    if (appName !== this.ALL_APPS) {
      if (this.descriptors.has(namespace)) {
        if (this.descriptors.get(namespace).has(appName)) {
          this.descriptors.get(namespace).set(appName, []);
        }
      } else {
        let allApps = new Map<string, Descriptor[]>();
        this.descriptors.set(namespace, allApps);
      }
    } else {
      let allApps = new Map<string, Descriptor[]>();
      this.descriptors.set(namespace, allApps);
    }
    let apps = this.descriptors.get(namespace);

    return this._authHttp.get(
      this.host + '/descriptors?namespace=' + namespace + (appName !== this.ALL_APPS ? "&appname=" + appName : "")
    )
      .map(res => res.json())
      .flatMap(d => {
        let descriptors = <Descriptor[]>d;

        for (let descriptor of descriptors) {
          let appName = descriptor.appName;
          if (!apps.has(appName)) {
            apps.set(appName, []);
          }
          apps.get(appName).push(descriptor);
        }
        return Observable.of(apps);
      })
      .catch(err => {
        if (err.toString().indexOf('404') >= 0) {
          // no deployment for namespace, that's ok
        } else {
          this._logger.error('Error getting deployments for namespace ' + namespace, err);
        }
        return Observable.of(apps);
      });
  }

  getDescriptor(namespace: string, id: string): Observable<Descriptor> {
    this._logger.log('getting descriptor with id ' + id);
    return this.getDescriptorInternal(namespace, id)
      .defaultIfEmpty(undefined)
      .flatMap(d => {
        if (!d) {
          this._logger.log('descriptor not found, (re)loading descriptors from backend');
          return this.getDescriptors(namespace, true)
            .last()
            .flatMap(_ => this.getDescriptorInternal(namespace, id));
        }
        this._logger.log('cached descriptor found!');
        return Observable.of(d);
      });
  }

  deleteDescriptors(namespace: string, appName: string, keepLatest: boolean): Observable<string> {

    let url = this.host + '/descriptors'
      + '?namespace=' + namespace
      + '&appname=' + appName
      + '&keepLatest=' + keepLatest;

    return this._authHttp.delete(url)
      .map(res => res.statusText);
  }

  validate(descriptor: string): Observable<string> {
    return this._authHttp.post(
      this.host + '/descriptors/validate',
      descriptor
    )
      .map(res => res.text());
  }

  getDeployments(namespace: string, reload: boolean, limit: boolean): Observable<Map<string, Deployment[]>> {
    return this.getDeploymentsWithAppname(namespace, this.ALL_APPS, reload, limit);
  }

  getDeploymentsWithAppname(namespace: string, appName: string, reload: boolean, limit: boolean): Observable<Map<string, Deployment[]>> {
    if (!reload && this.deployments.has(namespace)) {
      return Observable.of(this.deployments.get(namespace));
    }

    // cache deployments
    if (appName !== this.ALL_APPS) {
      if (this.deployments.has(namespace)) {
        if (this.deployments.get(namespace).has(appName)) {
          this.deployments.get(namespace).set(appName, []);
        }
      } else {
        let allApps = new Map<string, Deployment[]>();
        this.deployments.set(namespace, allApps);
      }
    } else {
      let allApps = new Map<string, Deployment[]>();
      this.deployments.set(namespace, allApps);
    }
    let apps = this.deployments.get(namespace);

    return this._authHttp.get(
      this.host + '/deployments?namespace=' + namespace + (limit ? "&limit=5" : "") + (appName !== this.ALL_APPS ? "&appname=" + appName : "")
    )
      .map(res => res.json())
      .flatMap(d => {
        let deployments = <Deployment[]>d;

        for (let deployment of deployments) {
          let appName = deployment.descriptor.appName;
          if (!apps.has(appName)) {
            apps.set(appName, []);
          }
          apps.get(appName).push(deployment);
        }
        return Observable.of(apps);
      })
      .catch(err => {
        if (err.toString().indexOf('404') >= 0) {
          // no deployment for namespace, that's ok
        } else {
          this._logger.error('Error getting deployments for namespace ' + namespace, err);
        }
        return Observable.of(apps);
      });
  }

  getDeployment(namespace: string, id: string, forceReload: boolean): Observable<Deployment> {

    if (forceReload) {
      this._logger.log('forced reload, (re)loading deployments from backend');
      return this.reloadAndGetDeployment(namespace, id);
    }

    this._logger.log('getting deployment with id ' + id);
    return this.getDeploymentInternal(namespace, id)
      .defaultIfEmpty(undefined)
      .flatMap(d => {
        if (!d) {
          this._logger.log('deployment not found, (re)loading deployments from backend');
          return this.reloadAndGetDeployment(namespace, id);
        }
        this._logger.log('cached deployment found!');
        return Observable.of(d);
      });
  }

  deleteDeployments(namespace: string, appName: string, alsoUndeploy: boolean): Observable<string> {

    let url = this.host + '/deployments'
      + '?namespace=' + namespace
      + '&appname=' + appName
      + '&undeploy=' + alsoUndeploy;

    return this._authHttp.delete(url)
      .map(res => res.statusText);
  }

  deploy(namespace: string, descriptorId: string): Observable<string> {

    let url = this.host + '/deployments'
      + '?namespace=' + namespace
      + '&descriptorId=' + descriptorId;

    return this._authHttp.post(url, '')
      .map(res => this.getIdFromResponse(res));
  }

  redeploy(deployment: Deployment): Observable<string> {

    let url = this.host + '/deployments/' + deployment.id
      + '?namespace=' + deployment.descriptor.namespace;

    return this._authHttp.put(url, '')
      .map(res => this.getIdFromResponse(res));
  }

  undeploy(deployment: Deployment, alsoDelete: boolean): Observable<string> {

    let url = this.host + '/deployments/' + deployment.id
      + '?namespace=' + deployment.descriptor.namespace
      + '&deleteDeployment=' + alsoDelete;

    return this._authHttp.delete(url)
      .map(res => res.statusText);
  }

  getHealthdata(deployment: Deployment): Observable<HealthData[]> {
    return this._authHttp.get(
        this.host + '/deployments/' + deployment.id + '/healthcheckdata?namespace=' + deployment.descriptor.namespace
      )
      .map(res => {
        if (res.text().length > 0) {
          let data = res.json();
          for (let pod of data) {
            pod.value = JSON.stringify(JSON.parse(pod.value), null, '  ');
          }
          return data;
        } else {
          return [];
        }
      });
  }

  getLogs(deployment: Deployment): Observable<string> {
    return this._authHttp.get(
        this.host + '/deployments/' + deployment.id + '/logs?namespace=' + deployment.descriptor.namespace
      )
      .map(res => {
        if (res.text().length > 0) {
          return res.text();
        } else {
          return '';
        }
      });
  }

  streamLogs(deployment: Deployment): Observable<string> {
    return Observable.create(observer => {

      let topicId = 'logs-' + UUID.UUID();

      this._eventsService.registerTopic(EventsService.TOPIC_LOGS + topicId, false).subscribe(

        ev => {
            this._logger.log(ev);

            if (ev === '!!completed') {
              observer.complete();
            } else if (ev.startsWith('!!error')) {
              observer.error('Error: ' + ev.substring(7));
            } else {
              observer.next(ev);
            }
        },
        err => observer.error(err),
        () => observer.complete()
      );

      let url = this.host + '/stream/deployments/' + deployment.id
        + '/logs?namespace=' + deployment.descriptor.namespace + '&topicId=' + topicId;

      this._authHttp.get(url)
        .map(res => res.statusText)
        .subscribe(
          data => this._logger.log('stream logs request status: ' + data),
          err => this._logger.error('Error during streaming logs', err),
          () => this._logger.log('stream logs request complete')
        );

    });
  }

  private reloadAndGetDeployment(namespace: string, id: string): Observable<Deployment> {
    return this.getDeployments(namespace, true, true)
      .last()
      .flatMap(_ => this.getDeploymentInternal(namespace, id));
  }

  private getDeploymentInternal(namespace: string, id: string): Observable<Deployment> {
    if (!this.deployments.has(namespace)) {
      return Observable.from([]);
    }
    return Observable.of(this.deployments.get(namespace))
      .flatMap(appsMap => {
        let d: Deployment[] = [];
        for (let da of appsMap.values()) {
          d = d.concat(da);
        }
        return Observable.from(d);
      })
      .find(d => d.id === id)
  }

  private getDescriptorInternal(namespace: string, id: string): Observable<Descriptor> {
    if (!this.descriptors.has(namespace)) {
      return Observable.from([]);
    }
    return Observable.of(this.descriptors.get(namespace))
      .flatMap(appsMap => {
        let d: Descriptor[] = [];
        for (let da of appsMap.values()) {
          d = d.concat(da);
        }
        return Observable.from(d);
      })
      .find(d => d.id === id)
  }

  private getIdFromResponse(res: Response): string {
    let headers = res.headers;
    let location = headers.get('Location');
    let url = new URL(this.host + location);
    let path = url.pathname;
    if (path.endsWith('/')) {
      path = path.substring(0, path.length - 1);
    }
    let id = path.substring(path.lastIndexOf('/') + 1);
    return id;
  }

}
