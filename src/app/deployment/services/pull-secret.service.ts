/**
 @license Licensed under Apache License v2. See LICENSE for more information.
 */
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { ImagePullSecret } from '../models/deployment';
import { SecretService } from '../../secrets/services/secret.service';
import { NamespaceSecrets, SecretType } from '../../secrets/models/secret';

@Injectable()
export class PullSecretService {

  constructor(private _secretService: SecretService) {
  }

  getPullSecrets(namespace: string): Observable<ImagePullSecret[]> {
    return this._secretService.getNamespaceSecrets(namespace)
      .map(secrets => this.mapToImagePullSecret(secrets));
  }

  private mapToImagePullSecret(namespaceSecrets: NamespaceSecrets): ImagePullSecret[] {
    let pullSecrets: ImagePullSecret[] = [];
    namespaceSecrets.secrets
      .filter(secret => secret.type === SecretType.DOCKER_CONFIG.toString())
      .forEach(secret => pullSecrets.push(<ImagePullSecret>{'name': secret.metadata.name}));
    return pullSecrets;
  }
}
