/**
 @license Licensed under Apache License v2. See LICENSE for more information.
 */
export interface DeploymentStatus {
  success: boolean;
  ts?: string;
  podstatus?: string;
}

export interface HealthData {
  podName: string;
  value: string;
}

export interface Deployment {
  id: string;
  created: string;
  lastModified: string;
  status: string;
  descriptor: Descriptor;
}

export interface Descriptor {
  id: string;
  created: string;
  lastModified: string;
  webhooks?: WebHook[];
  deploymentType: string;
  namespace: string;
  useHealthCheck: boolean;
  healthCheckPath: string;
  healthCheckPort: number;
  healthCheckType: string;
  ignoreHealthCheck: boolean;
  newVersion: string;
  appName: string;
  replicas: number;
  frontend?: string;
  redirectWww?: boolean;
  podspec: PodSpec;
  useCompression?: boolean;
  useStickySessions?: boolean;
  additionHttpHeaders?: HttpHeader[];
  readinessPort?: number;
}

// workaround for enum with string values
export class DeploymentType {

  // values
  static BLUEGREEN = new DeploymentType('blue-green');

  constructor(public value: string) {
  }

  toString() {
    return this.value;
  }

}

export interface WebHook {
  key: string;
  description?: string;
}

export interface HttpHeader {
  Header: string;
  Value: string;
}

export interface PodSpec {
  imagePullSecrets?: ImagePullSecret[];
  containers: Container[];
  volumes?: Volume[];
}

export interface ImagePullSecret {
  name: string;
}

export interface Container {
  image: string;
  name: string;
  ports?: Port[];
  volumeMounts?: VolumeMount[];
  imagePullSecrets?: ImagePullSecret[];
  imagePullPolicy?: string;
  lifecycle?: LifeCycle;
  env?: Env[];
  resources?: Resources;
  readinessProbe?: Probe;
}

export interface Port {
  name: string;
  containerPort: number;
}

export interface VolumeMount {
  mountPath: string;
  name: string;
  readOnly: boolean;
}

export interface LifeCycle {
  postStart: Exec;
}

export interface Env {
  name: string;
  value: string;
}

export interface Resources {
  requests?: Resource;
  limits?: Resource;
}

export interface Resource {
  cpu?: string;
  memory?: string;
}

// workaround for enum with string values
export class ImagePullPolicy {

  // values
  static ALWAYS = new ImagePullPolicy('Always');
  static IF_NOT_PRESENT = new ImagePullPolicy('IfNotPresent');
  static NEVER = new ImagePullPolicy('Never');

  constructor(public value: string) {
  }

  toString() {
    return this.value;
  }

}


export interface Exec {
  command: string;
}

export interface Volume {
  name: string;
  emptyDir: EmptyDir;
}

export interface EmptyDir {

}

export interface Probe {
  httpGet: HttpProbe;
  initialDelaySeconds?: number;
  periodSeconds?: number;
  timeoutSeconds?: number;
  successThreshold?: number;
  failureThreshold?: number;
}

export interface HttpProbe {
   path: string;
   port: number;
}

// workaround for enum with string values
export class CpuLimitUnit {

  // values
  static MILLI_CPU = new CpuLimitUnit('m', 'Milli-CPUs');
  static FULL_CPU = new CpuLimitUnit('', 'CPUs');

  static getAll(): CpuLimitUnit[] {
    return [CpuLimitUnit.MILLI_CPU, CpuLimitUnit.FULL_CPU];
  }

  constructor(public value: string,
              public name: string) {
  }

  toString() {
    return this.value;
  }

  getName() {
    return this.name;
  }

}

// workaround for enum with string values
export class MemoryLimitUnit {

  // values
  static MBYTE = new MemoryLimitUnit('Mi', 'MByte');
  static GBYTE = new MemoryLimitUnit('Gi', 'GByte');

  static getAll(): MemoryLimitUnit[] {
    return [MemoryLimitUnit.MBYTE, MemoryLimitUnit.GBYTE];
  }

  constructor(public value: string,
              public name: string) {
  }

  toString() {
    return this.value;
  }

  getName() {
    return this.name;
  }

}

// workaround for enum with string values
export class HealthCheckType {

  // values
  static SIMPLE = new HealthCheckType('simple');
  static PROBE = new HealthCheckType('probe');

  static getAll(): HealthCheckType[] {
    return [HealthCheckType.SIMPLE, HealthCheckType.PROBE];
  }

  constructor(public value: string) {
  }

  toString() {
    return this.value;
  }

}
