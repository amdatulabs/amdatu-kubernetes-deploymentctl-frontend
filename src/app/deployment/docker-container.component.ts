/**
 @license Licensed under Apache License v2. See LICENSE for more information.
 */
import { Component, OnInit, Input, Output, EventEmitter, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { UUID } from 'angular2-uuid';
import { Logger } from '../util/logger';
import { ResourceDefinition } from '../util/resourceLimit';
import {
  Descriptor,
  Container,
  Port,
  Env,
  ImagePullPolicy,
  Resource,
  Resources,
  CpuLimitUnit,
  MemoryLimitUnit,
  HealthCheckType, VolumeMount
} from './models/deployment';
import { Repository } from '../dockerHub/models/repository';
import { DockerHubService } from '../dockerHub/services/dockerHub.service';
import { DockerImage } from '../util/dockerimage';
import { DescriptorDetailsComponent } from './descriptor-details.component';

@Component({
  selector: 'docker-container',
  template: require('./views/docker-container.component.html')
})

export class DockerContainerComponent implements OnInit, AfterViewInit {

  private healthCheckPortName = 'healthcheck';
  private healthCheckPort: number = 9999;

  @Input() private descriptor: Descriptor;
  @Input() private container: Container;
  @Input() private isNew: boolean;
  @Input() private fromDeployment: boolean;
  @Input() private index: number;
  @Input() private parentForm: FormGroup;
  @Input() private refreshImagesNotifier: EventEmitter<any>;
  @Input() private refreshVolumeNotifier: EventEmitter<any>;
  @Output() private deleted = new EventEmitter();

  private repositories: Repository[] = [];
  private selectedRepository: Repository;
  private selectedRepositoryName: string;
  private selectedTagName: string;

  private formName: string;
  private form: FormGroup;

  private cpulimit: number;
  private cpulimitUnit: string;
  private memorylimit: number;
  private memorylimitUnit: string;

  private cpurequest: number;
  private cpurequestUnit: string;
  private memoryrequest: number;
  private memoryrequestUnit: string;

  private volumeEnabled: boolean = false;
  private volumePath: string = '';

  private pullPolicies: ImagePullPolicy[] =
    [ImagePullPolicy.ALWAYS, ImagePullPolicy.IF_NOT_PRESENT, ImagePullPolicy.NEVER];
  private cpuUnits = CpuLimitUnit.getAll();
  private memoryUnits = MemoryLimitUnit.getAll();
  private healthCheckTypes = HealthCheckType.getAll();

  constructor(private _dockerHubService: DockerHubService,
              private _logger: Logger,
              private _fb: FormBuilder) {
    this.form = this._fb.group({
      containername: [],
      image: [],
      tag: [],
      imagetag: [],
      pullpolicy: [],
      healthcheck: this._fb.group({
        useHealthCheck: [],
        healthCheckPath: [],
        healthCheckPort: [],
        healthCheckType: [],
        ignoreHealthCheck: []
      }),
      cpulimit: [],
      cpulimitUnit: [],
      memorylimit: [],
      memorylimitUnit: [],
      cpurequest: [],
      cpurequestUnit: [],
      memoryrequest: [],
      memoryrequestUnit: [],
      volumePath: [],
      stdin: []
    });
  }

  ngOnInit() {

    // create resources if not there yet
    if (!this.container.resources) {
      this.container.resources = <Resources>{};
    }
    // create resource limits if not there yet
    if (!this.container.resources.limits) {
      this.container.resources.limits = <Resource>{};
    }
    if (!this.container.resources.limits.cpu) {
      this.container.resources.limits.cpu = '500m';
    }
    if (!this.container.resources.limits.memory) {
      this.container.resources.limits.memory = '256Mi';
    }
    // create resource requests if not there yet
    if (!this.container.resources.requests) {
      this.container.resources.requests = <Resource>{};
    }
    if (!this.container.resources.requests.cpu) {
      this.container.resources.requests.cpu = '250m';
    }
    if (!this.container.resources.requests.memory) {
      this.container.resources.requests.memory = '128Mi';
    }

    if (!this.container.ports) {
      this.container.ports = [];
    }
    if (!this.container.env) {
      this.container.env = [];
    }

    this.parseLimits();

    this.loadRepositories();

    this.formName = UUID.UUID();
    if (!this.parentForm.controls[this.formName]) {
      this._logger.log('adding container control ' + this.formName);
      this.parentForm.addControl(this.formName, this.form);
    }

    if (this.descriptor.healthCheckPort) {
      this.healthCheckPort = this.descriptor.healthCheckPort;
    }
    this.healthCheckUpdated();

    this.refreshImagesNotifier.subscribe(_ => {
      this._logger.log('got refresh images notification');
      this.loadRepositories();
    });

    if (this.descriptor.podspec.volumes) {
      let volume = this.descriptor.podspec.volumes.find(v => v.name === DescriptorDetailsComponent.VOLUME_NAME);
      if (volume) {
        this.volumeEnabled = true;
      }
    }
    this.initVolumePath();
    this.refreshVolumeNotifier.subscribe(
      enabled => {
        this.volumeEnabled = enabled;
        this.initVolumePath();
      }
    );
  }

  ngAfterViewInit() {
    // init bootstrap popovers
    $('[data-toggle="popover"]').popover({html: true});
  }

  private parseLimits() {
    let s = this.container.resources.limits.cpu;
    let definition = ResourceDefinition.createFromString(s);
    this.cpulimit = definition.value;
    this.cpulimitUnit = definition.unit;

    s = this.container.resources.requests.cpu;
    definition = ResourceDefinition.createFromString(s);
    this.cpurequest = definition.value;
    this.cpurequestUnit = definition.unit;

    s = this.container.resources.limits.memory;
    definition = this.getMemoryResourceDefinition(s);
    this.memorylimit = definition.value;
    this.memorylimitUnit = definition.unit;

    s = this.container.resources.requests.memory;
    definition = this.getMemoryResourceDefinition(s);
    this.memoryrequest = definition.value;
    this.memoryrequestUnit = definition.unit;
  }

  private getMemoryResourceDefinition(s: string): ResourceDefinition {
    let definition = ResourceDefinition.createFromString(s);
    // prevent the need of having too many items in the unit selection list
    if (!definition.unit || definition.unit === '') {
      // Bytes to MBytes
      definition.value /= 1024 * 1024;
      definition.unit = 'Mi';
    } else if (definition.unit === 'm') {
      // milli bytes to MBytes
      definition.value /= 1024 * 1024 * 1024;
      definition.unit = 'Mi';
    } else if (definition.unit === 'K') {
      // KBytes to MBytes
      definition.value /= 1024;
      definition.unit = 'Mi';
    } else if (definition.unit.length === 1) {
      // M / G (1000 based) to Mi / Gi (1024 based)
      definition.unit = definition.unit + 'i';
    }
    return definition;
  }

  private updateLimits() {
    this.container.resources.limits.cpu = this.cpulimit + this.cpulimitUnit;
    this.container.resources.limits.memory = this.memorylimit + this.memorylimitUnit;
  }

  private updateRequests() {
    this.container.resources.requests.cpu = this.cpurequest + this.cpurequestUnit;
    this.container.resources.requests.memory = this.memoryrequest + this.memoryrequestUnit;
  }

  private initVolumePath() {
    if (this.volumeEnabled) {
      if (this.container.volumeMounts) {
        let volumeMount = this.container.volumeMounts.find(m => m.name === DescriptorDetailsComponent.VOLUME_NAME);
        if (volumeMount) {
          this.volumePath = volumeMount.mountPath;
        }
      }
    } else {
      this.volumePath = '';
    }
  }

  private loadRepositories() {
    this.repositories = this._dockerHubService.getRepositories();
    if (this.container.image) {
      let docker = DockerImage.createFromString(this.container.image);
      if (docker.image && docker.tag) {
        this.selectedRepositoryName = docker.image;
        this.selectedTagName = docker.tag;
        this.onRepositorySelected();
        this.onTagSelected();
      }
    }
  }

  private onRepositorySelected() {

    this._logger.log('onRepositorySelected: ' + this.selectedRepositoryName);

    if (!this.selectedRepositoryName) {
      this.setContainerImage();
      return;
    }

    if (this.repositories && this.repositories.length > 0) {
      this.selectedRepository = this.repositories.find(
        r => r.fullname == this.selectedRepositoryName
      );
      if (this.selectedRepository && this.selectedRepository.tags && this.selectedRepository.tags.length > 0) {
        // we found a repository with known tags
        // if tagname is empty and repository has 1 tag only, select that one
        if (!this.selectedTagName) {
          this.selectedTagName = this.selectedRepository.tags[0].name;
        }
      }
    }

    this.setContainerImage();
  }

  private onTagSelected() {

    this._logger.log('onTagSelected: ' + this.selectedTagName);

    if (!this.selectedTagName) {
      this.setContainerImage();
      return;
    }

    if (this.selectedRepository && this.selectedRepository.tags) {
      // if tag was selected from loaded repositories, we have the fullname (name + last updated)
      // but we need name only
      let tag = this.selectedRepository.tags.find(
        t => t.fullname == this.selectedTagName
      );
      if (tag) {
        this.selectedTagName = tag.name;
      }
    }

    this.setContainerImage();
  }

  private setContainerImage() {
    if (this.selectedRepositoryName) {
      let image = this.selectedRepositoryName;
      if (this.selectedTagName) {
        image += ":" + this.selectedTagName;
      }
      this.container.image = image;
    } else {
      this.container.image = '';
    }
  }

  private onVolumePathChange() {
    this.container.volumeMounts = [];
    if (this.volumePath) {
      if (!this.volumePath.startsWith('/')) {
        this.volumePath = '/' + this.volumePath;
      }
      let volumeMount = <VolumeMount>{name: DescriptorDetailsComponent.VOLUME_NAME, mountPath: this.volumePath, readOnly: false};
      this.container.volumeMounts.push(volumeMount);
    }
  }

  private removeThis() {
    this._logger.log('removing control ' + this.formName);
    this.parentForm.removeControl(this.formName);
    this.deleted.emit('');
  }

  private addPort() {
    this._logger.log('addPort');
    this.container.ports.push(<Port>{});
  }

  private onPortDeleted(port: Port) {
    this.container.ports.splice(this.container.ports.indexOf(port), 1);
    this.form.updateValueAndValidity({onlySelf: false, emitEvent: true});
  }


  private addEnv() {
    this._logger.log('addEnv');
    this.container.env.push(<Env>{});
  }

  private onEnvDeleted(env: Env) {
    this.container.env.splice(this.container.env.indexOf(env), 1);
    this.form.updateValueAndValidity({onlySelf: false, emitEvent: true});
  }

  private healthCheckUpdated() {
    // if healthcheck is enabled, add port to container ports
    // else remove it from container ports
    // only on first container

    if (this.descriptor.podspec.containers[0] !== this.container) {
      return;
    }

    let port: Port;
    if (this.container.ports) {
      port = this.container.ports.find(p => p.name === this.healthCheckPortName);
    } else {
      this.container.ports = [];
    }

    if (this.descriptor.useHealthCheck) {
      // add port
      if (!port) {
        port = <Port>{'name': this.healthCheckPortName};
        this.container.ports.push(port);
      }
      port.containerPort = this.healthCheckPort;
      this.descriptor.healthCheckPort = this.healthCheckPort;
    } else {
      if (port) {
        this.container.ports.splice(this.container.ports.indexOf(port), 1);
      }
    }
  }

}
