/**
 @license Licensed under Apache License v2. See LICENSE for more information.
 */
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Logger } from '../util/logger';
import { Deployment, HealthData } from './models/deployment';
import { DeploymentService } from './services/deployment.service';

@Component({
  selector: 'deployment-details',
  template: require('./views/deployment-details.component.html')
})

export class DeploymentDetailsComponent implements OnInit {

  private deployment: Deployment;
  private logs: string = '';
  private healthData: HealthData[] = [];
  private isBusy: boolean = false;
  private isDeleted: boolean = false;
  private isLoggedIn = false;
  private doReload: boolean = false;
  private doPurge: boolean = false;

  @ViewChild('logView') logView;

  constructor(private _deploymentService: DeploymentService,
              private _logger: Logger,
              private _router: Router,
              private _route: ActivatedRoute) {
  }

  ngOnInit() {

    this._route
      .params
      .subscribe(params => {

        let namespace = params['namespace'];
        let id = params['id'];

        if (namespace && id) {
          this._deploymentService.getDeployment(namespace, id, false).subscribe(
            deployment => {
              this.deployment = deployment;
              if (deployment.status === 'DEPLOYING' || deployment.status === 'UNDEPLOYING') {
                this.doReload = true;
              }
              this.streamLogs();
            },
            err => this._logger.error('Error getting deployment', err),
            () => {}
          );
        } else {
          this._logger.log('no id found, redirecting to list');
          this._router.navigate(['/listApplications']);
        }

        let purge = params['purge'];
        if (purge) {
          this.doPurge = true;
          this.onUndeploy();
        }

      });
  }

  private onUndeploy() {
    this._deploymentService.undeploy(this.deployment, false)
      .delay(200)
      .subscribe(
        _ => {
          this._logger.info('Undeployment started');
          this.reload();
        },
        err => this._logger.error('Error undeploying: ', err)
      );
  }

  private onDelete() {
    this._deploymentService.undeploy(this.deployment, true)
      .subscribe(
        _ => this._logger.info('Deployment deleted'),
        err => this._logger.error('Error deleting deployment: ', err),
        () => this._router.navigate(['/listApplications']),
      );
  }

  private onRedeploy() {

    this._deploymentService.redeploy(this.deployment)
      .delay(200)
      .subscribe(
        id => {
          // for some unknown reason this is not cleared when navigating to the same page, but with another id...
          this.logs = '';
          this.healthData = [];
          this._logger.info('Redeployment started');
          this._router.navigate(['/deployment', {namespace: this.deployment.descriptor.namespace, appName: this.deployment.descriptor.appName, id: id}])
        },
        err => this._logger.error('Error redeploying: ', err)
      );

  }

  private onShowDescriptor() {
    this._router.navigate(
      ['/descriptor', { namespace: this.deployment.descriptor.namespace, appName: this.deployment.descriptor.appName, deploymentId: this.deployment.id}]
    );
  }

  private streamLogs() {
    this.logs = '';
    this._deploymentService.streamLogs(this.deployment).subscribe(
        logs => {
          this.isBusy = true;
          this.logs += logs;
          if (this.logView) {
            // scroll down
            let el = this.logView.nativeElement;
            setTimeout(() => el.scrollTop = el.scrollHeight, 0);
          }
        },
        err => {
          if (err.toLocaleString().indexOf('Key not found') >= 0) {
            this.logs = '';
          } else {
            this._logger.error('Error getting logs', err);
          }
          this.isBusy = false;
          if (this.doReload) {
            this.reload();
          } else {
            this.getHealthData();
          }
        },
        () => {
          this.isBusy = false;
          if (this.doReload) {
            this.reload();
          } else {
            if (this.logView) {
              // scroll down
              let el = this.logView.nativeElement;
              setTimeout(() => el.scrollTop = el.scrollHeight, 0);
            }
            this.getHealthData();
          }
        }
    );
  }

  private reload() {
    this.doReload = false;
    this._deploymentService.getDeployment(this.deployment.descriptor.namespace, this.deployment.id, true).subscribe(
      d => {
        this.deployment = d;
        if (this.deployment.status === 'DEPLOYING' || this.deployment.status === 'UNDEPLOYING') {
          this.doReload = true;
        } else if (this.deployment.status === 'UNDEPLOYED' && this.doPurge) {
          this.onDelete();
        }
        this.streamLogs();
      },
      err => this._logger.error('Error reloading deployment', err)
    );
  }

  private getHealthData() {
    this.healthData = [];
    this._deploymentService.getHealthdata(this.deployment).subscribe(
          data => {
            this.healthData = data;
          },
          err => {
            if (err.toLocaleString().indexOf('404') >= 0) {
              this._logger.log('No health data found: ' + err.toString());
            } else {
              this._logger.error('Error getting health data', err);
            }
          },
          () => {}
    );
  }
}

