/**
 @license Licensed under Apache License v2. See LICENSE for more information.
 */
import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { Logger } from '../util/logger';
import { Deployment, Descriptor } from './models/deployment';
import { DeploymentService } from './services/deployment.service';
import { ConfirmDialogComponent } from '../util/confirm-dialog.component';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { WaitDialogComponent } from "app/util/wait-dialog.component";
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'application-list',
  template: require('./views/application-list.component.html')
})

export class ApplicationListComponent implements OnInit {

  private readonly CONCURRENT_DELETES = 3;

  // map of namespace -> appname -> DESCRIPTORS / DEPLOYMENTS -> Descriptor[] / Deployment[]
  private items: Map<string, Map<string, Map<string, any>>> = new Map<string, Map<string, Map<string, any>>>();
  private readonly DESCRIPTORS = "descriptors";
  private readonly DEPLOYMENTS = "deployments";

  private isLoggedIn = false;
  private isLoading = false;
  private namespace: string;
  private appName: string;

  @ViewChild(ConfirmDialogComponent) confirmDialog:ConfirmDialogComponent;
  @ViewChild(WaitDialogComponent) waitDialog:WaitDialogComponent;

  constructor(private _logger: Logger,
              private _deploymentService: DeploymentService,
              private _router: Router,
              private _route: ActivatedRoute) {
  }

  ngOnInit() {
    this._route.params.subscribe(params => {
      this.namespace = params['namespace'];
      this.appName = params['appName'];

      this.init(true);
    });
  }

  private init(limit: boolean) {

    this.isLoading = true;
    let loadingCount = 0;
    this.items = new Map<string, Map<string, Map<string, any>>>();

    this._deploymentService.getNamespaces()
      // sort namespaces here already in order to not shuffle around in the UI
      // does not work 100% because of async loading of descriptors / deployments, but it's better than nothing
      .toArray()
      .map(ns => ns.sort((ns1, ns2) => ns1.localeCompare(ns2)))
      .flatMap(ns => Observable.from(ns))
      .subscribe(
        ns => {

          loadingCount++;

          if (!this.items.has(ns)) {
            this.items.set(ns, new Map<string, Map<string, any>>());
          }

          Observable.merge(
            this._deploymentService.getDescriptors(ns, false),
            this._deploymentService.getDeployments(ns, false, limit)
          ).subscribe(
            map => {
              for (let appName of map.keys()) {

                if (!this.items.get(ns).has(appName)) {
                  let app: Map<string, any> = new Map<string, any>();
                  app.set(this.DESCRIPTORS, <Descriptor[]>[]);
                  app.set(this.DEPLOYMENTS, <Deployment[]>[]);
                  this.items.get(ns).set(appName, app)
                }

                let d = map.get(appName);
                if (d.length > 0) {
                  if ((<Descriptor[]>d)[0].appName) {
                    let descriptors: Descriptor[] = (<Descriptor[]>d);
                    descriptors.sort((d1, d2) => d2.lastModified.localeCompare(d1.lastModified));
                    this.items.get(ns).get(appName).set(this.DESCRIPTORS, descriptors);
                  }
                  else if ((<Deployment[]>d)[0].status) {
                    let deployments: Deployment[] = (<Deployment[]>d);
                    deployments.sort((d1, d2) => d2.lastModified.localeCompare(d1.lastModified));
                    this.items.get(ns).get(appName).set(this.DEPLOYMENTS, deployments);
                  }
                }

              }
            },
            err => {
              this._logger.error('Error getting descriptors / deployments', err);
              loadingCount --;
              this.isLoading = loadingCount > 0;
            },
            () => {
              loadingCount --;
              this.isLoading = loadingCount > 0;
            }
          );

        },
        err => this._logger.error('Error getting descriptors / deployments', err),
        () => {}
      );
  }

  private onDeleteDeployment(deployment: Deployment) {
    this._deploymentService.undeploy(deployment, true)
      .subscribe(
        _ => {},
        err => this._logger.error('Error deleting deployment: ', err),
        () => {
          let deployments = this.items.get(deployment.descriptor.namespace).get(deployment.descriptor.appName).get(this.DEPLOYMENTS);
          deployments.splice(deployments.indexOf(deployment, 0), 1);
          this._logger.info("Deployment deleted")
        }
      );
  }

  private onDeleteDescriptor(descriptor: Descriptor) {
    this._deploymentService.deleteDescriptor(descriptor)
      .subscribe(
        _ => {},
        err => this._logger.error('Error deleting descriptor: ', err),
        () => {
          let descriptors = this.items.get(descriptor.namespace).get(descriptor.appName).get(this.DESCRIPTORS);
          descriptors.splice(descriptors.indexOf(descriptor, 0), 1);
          this._logger.info("Descriptor deleted")
        }
      );
  }

  // private onPurge(namespace: string, appName: string) {
  //   this.confirmDialog.showModal('Attention: This will undeploy a potentially deployed application and delete all descriptors and deployments! Are you sure?')
  //     .subscribe(
  //       ok => {
  //         // if (ok) {
  //         //   ...
  //         // }
  //       }
  //     );
  // }

  private onDeleteAllDeployments(namespace: string, appName: string) {
    this.confirmDialog.showModal('Attention: This will delete all undeployed and failed deployments! Are you sure?')
      .subscribe(
        ok => {
          if (ok) {

            this.waitDialog.showModal('Deleting deployments...');

            this._deploymentService.deleteDeployments(namespace, appName, false).subscribe(
              result => {
                this._logger.log("delete deployments result: " + result);
              },
              err => {
                this._logger.error("Error deleting deployments", err);
                this.waitDialog.hideModal();
              },
              () => {
                this.onLoadAllDeployments(namespace, appName);
                this.waitDialog.hideModal();
              }

            );

          }
        }
      );
  }

  private onDeleteAllDescriptors(namespace: string, appName: string) {
    this.confirmDialog.showModal('Attention: This will delete all but the last modified descriptors! Are you sure?')
      .subscribe(
        ok => {
          if (ok) {

            this.waitDialog.showModal('Deleting descriptors...');

            this._deploymentService.deleteDescriptors(namespace, appName, true).subscribe(
              result => {
                this._logger.log("delete descriptors result: " + result);
              },
              err => {
                this._logger.error("Error deleting descriptors", err);
                this.waitDialog.hideModal();
              },
              () => {
                this.onLoadAllDesriptors(namespace, appName);
                this.waitDialog.hideModal();
              }

            );

          }
        }
      );
  }

  private onLoadAllDeployments(namespace: string, appname: string) {
    this._deploymentService.getDeploymentsWithAppname(namespace, appname, true, false).subscribe(
      d => {
        let deployments = d.get(appname);
        deployments.sort((d1, d2) => d2.lastModified.localeCompare(d1.lastModified));
        this.items.get(namespace).get(appname).set(this.DEPLOYMENTS, deployments);
      },
      err => {this._logger.error("Error loading deployments", err)},
      () => {}
    )
  }

  private onLoadAllDesriptors(namespace: string, appname: string) {
    this._deploymentService.getDescriptorsWithAppname(namespace, appname, true).subscribe(
      d => {
        let descriptors = d.get(appname);
        descriptors.sort((d1, d2) => d2.lastModified.localeCompare(d1.lastModified));
        this.items.get(namespace).get(appname).set(this.DESCRIPTORS, descriptors);
      },
      err => {this._logger.error("Error loading deployments", err)},
      () => {}
    )
  }

  private getNamespacePanelClass(namespace: string) {
    let elemClass = 'panel-collapse collapse';

    if(this.namespace && this.namespace.localeCompare(namespace) === 0) {
      elemClass += ' in';
    }

    return elemClass;
  }

  private getApplicationPanelClass(namespace: string, appName: string) {
    let elemClass = 'panel-collapse collapse';

    if(this.namespace && this.namespace.localeCompare(namespace) === 0 && this.appName && this.appName.localeCompare(appName) === 0) {
      elemClass += ' in';
    }
    return elemClass;
  }

}
