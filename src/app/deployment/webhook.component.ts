/**
 @license Licensed under Apache License v2. See LICENSE for more information.
 */
import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { UUID } from 'angular2-uuid';
import { Logger } from '../util/logger';
import { Descriptor, WebHook } from './models/deployment';
import { Host } from '../util/host';

@Component({
  selector: 'webhook',
  template: require('./views/webhook.component.html')
})

export class WebhookComponent implements OnInit {

  @Input() private descriptor: Descriptor;
  @Input() private webhook: WebHook;
  @Input() private isNew: boolean;
  @Input() private fromDeployment: boolean;
  @Input() private index: number;
  @Input() private parentForm: FormGroup;
  @Output() private deleted = new EventEmitter();

  private formName: string;
  private form: FormGroup;
  private url: string;

  constructor(private _logger: Logger,
              private _fb: FormBuilder) {
    this.form = this._fb.group({
      webhookkey: [],
      description: []
    });
  }

  ngOnInit() {
    this.formName = UUID.UUID();
    if (!this.parentForm.controls[this.formName]) {
      this._logger.log('adding webhook control ' + this.formName);
      this.parentForm.addControl(this.formName, this.form);
    }
    this.url = Host.getBackendUrl() + '/webhooks/' + this.descriptor.namespace + '/' + this.webhook.key;
  }

  private removeThis() {
    this._logger.log('removing webhook control ' + this.formName);
    this.parentForm.removeControl(this.formName);
    this.deleted.emit('');
  }

}
