/**
 @license Licensed under Apache License v2. See LICENSE for more information.
 */
import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { UUID } from 'angular2-uuid';
import { Logger } from '../util/logger';
import { Deployment, WebHook, HttpHeader } from './models/deployment';
import { Host } from '../util/host';

@Component({
  selector: 'httpheader',
  template: require('./views/header.component.html')
})

export class HeaderComponent implements OnInit {

  @Input() private header: HttpHeader;
  @Input() private parentForm: FormGroup;
  @Input() private fromDeployment: boolean;
  @Output() private deleted = new EventEmitter();

  private formName: string;
  private form: FormGroup;

  constructor(private _logger: Logger,
              private _fb: FormBuilder) {
    this.form = this._fb.group({
      header: [],
      value: []
    });
  }

  ngOnInit() {
    this.formName = UUID.UUID();
    if (!this.parentForm.controls[this.formName]) {
      this._logger.log('adding header control ' + this.formName);
      this.parentForm.addControl(this.formName, this.form);
    }
  }

  private removeThis() {
    this._logger.log('removing header control ' + this.formName);
    this.parentForm.removeControl(this.formName);
    this.deleted.emit('');
  }

}
