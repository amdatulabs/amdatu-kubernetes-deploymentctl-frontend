/**
 @license Licensed under Apache License v2. See LICENSE for more information.
 */
import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { UUID } from 'angular2-uuid';
import { Logger } from '../util/logger';
import { Container, Env } from './models/deployment';

@Component({
  selector: 'docker-env',
  template: require('./views/docker-env.component.html')
})

export class DockerEnvComponent implements OnInit {

  @Input() private container: Container;
  @Input() private env: Env;
  @Input() private isNew: boolean;
  @Input() private fromDeployment: boolean;
  @Input() private index: number;
  @Input() private parentForm: FormGroup;
  @Output() private deleted = new EventEmitter();

  private formName: string;
  private form: FormGroup;

  constructor(private _logger: Logger,
              private _fb: FormBuilder) {
    this.form = this._fb.group({
      name: [],
      value: []
    });
  }

  ngOnInit() {

    this.formName = UUID.UUID();
    if (!this.parentForm.controls[this.formName]) {
      this._logger.log('adding env control ' + this.formName);
      this.parentForm.addControl(this.formName, this.form);
    }

  }

  private removeThis() {
    this._logger.log('removing env control ' + this.formName);
    this.parentForm.removeControl(this.formName);
    this.deleted.emit('');
  }

}
