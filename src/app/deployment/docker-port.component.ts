/**
 @license Licensed under Apache License v2. See LICENSE for more information.
 */
import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { UUID } from 'angular2-uuid';
import { Logger } from '../util/logger';
import { Container, Descriptor, Port, Probe } from './models/deployment';

@Component({
  selector: 'docker-port',
  template: require('./views/docker-port.component.html')
})

export class DockerPortComponent implements OnInit {

  @Input() private descriptor: Descriptor;
  @Input() private container: Container;
  @Input() private port: Port;
  @Input() private isNew: boolean;
  @Input() private fromDeployment: boolean;
  @Input() private healthCheckPortName: string;
  @Input() private index: number;
  @Input() private parentForm: FormGroup;
  @Output() private deleted = new EventEmitter();

  private formName: string;
  private form: FormGroup;
  private useReadiness: boolean;
  private readinessPath: string;

  constructor(private _logger: Logger,
              private _fb: FormBuilder) {
    this.form = this._fb.group({
      name: [],
      port: [],
      useReadiness: [],
      readinessPath: []
    });
  }

  ngOnInit() {

    if (this.port.name === this.healthCheckPortName) {
      // skip validation on healthcheck port, was added automatically
      // and already validated in healthcheck panel
      return;
    }

    this.formName = UUID.UUID();
    if (!this.parentForm.controls[this.formName]) {
      this._logger.log('adding port control ' + this.formName);
      this.parentForm.addControl(this.formName, this.form);
    }

    if (this.descriptor.readinessPort &&
      this.port.containerPort &&
      this.descriptor.readinessPort === this.port.containerPort) {

      this.useReadiness = true;
    }
    if (this.container.readinessProbe) {
      this.readinessPath = this.container.readinessProbe.httpGet.path;
    } else {
      this.readinessPath = '/';
    }
  }

  private removeThis() {

    this.removeReadiness();

    this._logger.log('removing port control ' + this.formName);
    this.parentForm.removeControl(this.formName);
    this.deleted.emit('');
  }

  private configureReadiness() {

    if (this.port && this.port.containerPort && this.useReadiness) {
      this.container.readinessProbe = {
        httpGet: {
          port: this.port.containerPort,
          path: this.readinessPath
        },
        initialDelaySeconds: 10,
        periodSeconds: 5,
        timeoutSeconds: 3,
        successThreshold: 1,
        failureThreshold: 1
      };
      this.descriptor.readinessPort = this.port.containerPort;
    } else {
      this.removeReadiness();
    }
  }

  private removeReadiness() {
    // if (this.container.readinessProbe &&
    //   this.port && this.port.containerPort &&
    //   this.container.readinessProbe.httpGet.port === this.port.containerPort) {

      delete this.container.readinessProbe;
      delete this.descriptor.readinessPort;
    // }
  }

}

