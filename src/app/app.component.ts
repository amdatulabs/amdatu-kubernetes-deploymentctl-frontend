/**
 * @license Licensed under Apache License v2. See LICENSE for more information.
 */
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { DockerHubService } from './dockerHub/services/dockerHub.service';
import { Logger } from './util/logger';
import { KeycloakService } from './keycloak/keycloak.service';
import { NotificationsService } from 'angular2-notifications/dist';

require('!style!css!./styles.css');

@Component({
  selector: 'app',
  encapsulation: ViewEncapsulation.None,
  template: require('./app.html')
})

export class AppComponent implements OnInit {

  private userInfo: any;
  private dockerUser: string;

  private notificationoptions = {
    position: ['bottom', 'right'],
    timeOut: 5000,
    preventDuplicates: true,
    preventLastDuplicates: 'visible',
    animate: 'scale'
  };

  constructor(private _dockerHubService: DockerHubService,
              private _logger: Logger,
              private _router: Router,
              private _keycloakService: KeycloakService,
              private _notificationService: NotificationsService,
  ) {}


  ngOnInit() {

    this._logger.getErrors().subscribe(
      msg => {
        this._notificationService.error("Error", msg);
      }
    );

    this._logger.getWarnings().subscribe(
      msg => {
        this._notificationService.warn("Warning", msg);
      }
    );

    this._logger.getInfos().subscribe(
      msg => {
        this._notificationService.success("Information", msg);
      }
    );

    this._dockerHubService.dockerLoggedIn$.subscribe(dockerLoggedIn => {
      if (dockerLoggedIn) {
        this.dockerUser = this._dockerHubService.dockerUsername;
      } else {
        this.dockerUser = '';
      }
    });

    this._keycloakService.getUserInfo().success(userInfo => {
      this.userInfo = userInfo;
      this._logger.log('user: ' + this.userInfo.email);
    }).error(function () {
      this._logger.log('failed to load userinfo');
    });

  }

  private logout() {
    this._dockerHubService.dockerLogout();
    this._keycloakService.logout();
  }

}
