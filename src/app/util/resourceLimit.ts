/**
 @license Licensed under Apache License v2. See LICENSE for more information.
 */
export class ResourceDefinition {

  static createFromString(s: string): ResourceDefinition {
    let definition = new ResourceDefinition();
    let regex = new RegExp('(\\d*\\.?\\d*)(\\D*)'); // digits, '.' or ',', digits, non-digits
    let match = regex.exec(s);
    definition.value = +match[1];
    if (match.length === 3) {
      definition.unit = match[2];
    }
    return definition;
  }

  constructor(
    public value: number = null,
    public unit: string = ''
  ) {
  }

}
