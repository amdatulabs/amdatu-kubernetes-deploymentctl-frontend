/**
 @license Licensed under Apache License v2. See LICENSE for more information.
 */
export class Base64 {
  static encode(s: string): string {
    return btoa(s);
  }

  static decode(s: string): string {
    return atob(s);
  }
}
