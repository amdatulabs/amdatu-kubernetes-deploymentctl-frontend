/**
 @license Licensed under Apache License v2. See LICENSE for more information.
 */
import { Component } from '@angular/core';

@Component({
  selector: 'spinner',
  template: `

<div id='spinnerModal' class='modal model-sm fade' role='dialog'>
  <div class='modal-dialog'>
    <div class='modal-content'>
      <div class='modal-body text-center'>
          <h3>{{text}}</h3>
          <br>
          <span style="font-size: 1.8em;" class="glyphicon glyphicon-refresh glyphicon-spin"></span>
      </div>
    </div>
  </div>
</div>

`

})

export class SpinnerComponent {

  private static instance: SpinnerComponent;
  private text: string;

  static show(text: string) {
    SpinnerComponent.instance.text = text;
    $('#spinnerModal').modal('show');
  }

  static hide() {
    $('#spinnerModal').modal('hide');
  }

  constructor() {
    SpinnerComponent.instance = this;
  }

}

