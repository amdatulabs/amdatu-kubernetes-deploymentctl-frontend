/**
 @license Licensed under Apache License v2. See LICENSE for more information.
 */
import { Directive, Input, ElementRef } from '@angular/core';
import { FormControl } from '@angular/forms';

/**
 * based on https://github.com/angular/angular/issues/11271#issuecomment-266314788
 */
@Directive({
  selector: '[formControlName][disabledCondition]'
})
export class DisableFormControlDirective {
  private el: any;

  constructor(el: ElementRef) {
    this.el = el.nativeElement;
  }

  @Input('disabledCondition') set disabledCondition(s: boolean) {
    if (!this.el) return;
    else if (s) this.el.disabled = true;
    else this.el.disabled = false;
  }
}
