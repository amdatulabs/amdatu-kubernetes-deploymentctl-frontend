/**
 @license Licensed under Apache License v2. See LICENSE for more information.
 */
import { AbstractControl, FormGroup, FormControl } from '@angular/forms';
import { Secret, SecretDataWrapper } from '../secrets/models/secret';

export class DctlValidators {

  private static REGEX_DNS = new RegExp('^[a-z]([-a-z0-9]*[a-z0-9])?$');
  private static REGEX_DNS_HASH_LEADING_NUMBER = new RegExp('^(#|[a-z0-9]([-a-z0-9]*[a-z0-9])?)$');
  private static REGEX_C_IDENT = new RegExp('^[A-Za-z_][A-Za-z0-9_]*$');

  public static checkboxInputValidator(checkboxName: string, inputName: string, errorCode: string) {
    return (controlGroup: FormGroup): {[key: string]: any} => {
      let checkbox = controlGroup.controls[checkboxName].value;
      let input = controlGroup.controls[inputName].value;
      if (checkbox && !input) {
        this.addError(inputName, controlGroup.controls[inputName], errorCode);
      } else {
        this.removeError(inputName, controlGroup.controls[inputName], errorCode);
      }
      return null;
    };
  }

  public static combinedLengthValidator(fieldnames: string[], maxlength: number, errorCode: string) {
    return (controlGroup: FormGroup): {[key: string]: any} => {
      let length = 0;
      for (let f of fieldnames) {
        let value = controlGroup.controls[f].value;
        if (value && value.length) {
          length += value.length;
        }
      }
      if (length > maxlength) {
        for (let f of fieldnames) {
          this.addError(f, controlGroup.controls[f], errorCode);
        }
      } else {
        for (let f of fieldnames) {
          this.removeError(f, controlGroup.controls[f], errorCode);
        }
      }
      return null;
    };
  }

  public static dnsValidator(allowHashAndLeadingNumber: boolean, errorMsg: string) {
    return (control: FormControl): {[key: string]: any} => {
      let value = control.value;
      if (value && value.length > 0 && allowHashAndLeadingNumber ?
          !this.REGEX_DNS_HASH_LEADING_NUMBER.test(value) : !this.REGEX_DNS.test(value)) {
        return {[errorMsg]: true};
      }
      return null;
    };
  }

  public static cIdentValidator(errorMsg: string) {
    return (control: FormControl): {[key: string]: any} => {
      let value = control.value;
      if (value && value.length > 0 && !this.REGEX_C_IDENT.test(value)) {
        return {[errorMsg]: true};
      }
      return null;
    };
  }

  public static uniqueSecretNamesValidator(secrets: Secret[], errorMsg: string) {
    return (control: FormControl): {[key: string]: any} => {
      let value = control.value;
      if (!value) {
        return;
      }
      if (secrets.filter(secret => secret.metadata.name === value).length > 1) {
        return {[errorMsg]: true};
      }
      return null;
    };
  }

  public static uniqueSecretDataKeysValidator(dataWrappers: SecretDataWrapper[], errorMsg: string) {
    return (control: FormControl): {[key: string]: any} => {
      if (!control.dirty) {
        return; // prevent validating readonly field
      }
      let value = control.value;
      if (!value) {
        return;
      }
      if (dataWrappers.filter(wrapper => wrapper.key === value).length > 1) {
        return {[errorMsg]: true};
      }
      return null;
    };
  }

  public static portValidator(errorMsg: string) {
    return (control: FormControl): {[key: string]: any} => {
      let value = control.value;
      if (value && (isNaN(value) || value < 1 || value > 65535)) {
        return {[errorMsg]: true};
      }
      return null;
    };
  }

  public static noProtocolValidator(errorMsg: string) {
    return (control: FormControl): {[key: string]: any} => {
      let value = control.value;
      if (value && value.indexOf('://') >= 0) {
        return {[errorMsg]: true};
      }
      return null;
    };
  }

  private static addError(name: string, control: AbstractControl, errorCode: string) {
    if (!control.hasError(errorCode)) {
      console.log('setting error ' + errorCode + ' to control ' + name);
      control.setErrors({[errorCode]: true});
    }
  }

  private static removeError(name: string, control: AbstractControl, errorCode: string) {
    if (control.hasError(errorCode)) {
      console.log('removing error ' + errorCode + ' from control ' + name);
      control.setErrors({});
      // a field which was not edited, but its error was removed, is not revalidated...
      control.updateValueAndValidity({onlySelf: false, emitEvent: true});
    }
  }
}
