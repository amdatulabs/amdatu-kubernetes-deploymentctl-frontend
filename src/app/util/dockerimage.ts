/**
 @license Licensed under Apache License v2. See LICENSE for more information.
 */
export class DockerImage {

  static createFromString(s: string): DockerImage {
    let docker = new DockerImage();
    let regex = new RegExp('(.*):(.*)');
    let match = regex.exec(s);
    if (match.length === 3) {
      docker.image = match[1];
      docker.tag = match[2];
    }
    return docker;
  }

  constructor(
    public image: string = '',
    public tag: string = ''
  ) {
  }

}
