import { Injectable } from "@angular/core";
import { Host } from './host';
import { Logger } from './logger';
import { Observable, Observer, Subject } from 'rxjs';
import { KeycloakService } from '../keycloak/keycloak.service';

interface Command {
  action: string
  value: string
}

@Injectable()
export class EventsService {

  public static readonly  TOPIC_LOGS = "org/amdatu/dctl/web/logs/"; // identifier will be appended

  private wsSubject: Subject<any>;

  constructor(
    private logger: Logger,
    private keycloak: KeycloakService
  ){

    document.cookie = 'deploymentctl-token=' + this.keycloak.getToken() + ';path=/;max-age=70';
    let ws = new WebSocket(Host.getBackendWebsocketUrl() + "events");

    let observable = Observable.create(
      (obs: Observer<any>) => {
        ws.onmessage = obs.next.bind(obs);
        ws.onerror = obs.error.bind(obs);
        ws.onclose = obs.complete.bind(obs);
        return ws.close.bind(ws);
      }
    ).share();

    let observer = {
      next: (data: Object) => {
        if (ws.readyState === WebSocket.OPEN) {
          ws.send(JSON.stringify(data));
        } else {
          ws.addEventListener('open', () => {
            ws.send(JSON.stringify(data));
          });
        }
      },
    };

    this.wsSubject = Subject.create(observer, observable);

    this.wsSubject.subscribe(
      next => {},//this.logger.log("received event: " + JSON.stringify(next.data)),
      err => this.logger.error("Error on event websocket connection!", err),
      () => this.logger.warn("Event websocket connection closed!")
    )

  }

  public registerTopic(topic: string, asJson: boolean): Observable<any> {
    this.logger.log('registering event topic: ' + topic);
    let cmd: Command = { action: 'registerTopic', value: topic };
    this.wsSubject.next(cmd);
    return this.wsSubject
      .map(e => {
        let eventData = e.data;
        // this.logger.log('data: ' + JSON.stringify(eventData));
        return JSON.parse(eventData);
      })
      .filter(data => {
        // this.logger.log('topic: ' + data.topic);
        // this.logger.log('requested topic: ' + topic);
        return data.topic == topic;
      })
      .map(data => asJson ? JSON.parse(data.payload) : data.payload);
  }

}

