/**
 @license Licensed under Apache License v2. See LICENSE for more information.
 */
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';

@Injectable()
export class Logger {

  private infoObservable: Observable<string>;
  private infoObserver: Observer<string>;
  private warnObservable: Observable<string>;
  private warnObserver: Observer<string>;
  private errorObservable: Observable<string>;
  private errorObserver: Observer<string>;

  constructor() {
    this.infoObservable = new Observable<string>(o => {
      this.infoObserver = o;
    });
    this.warnObservable = new Observable<string>(o => {
      this.warnObserver = o;
    });
    this.errorObservable = new Observable<string>(o => {
      this.errorObserver = o;
    });
  }

  log(msg: string) {
    console.log(msg);
  }

  info(msg: string) {
    console.info(msg);
    this.infoObserver.next(msg);
  }

  warn(msg: string, error?: any) {
    console.warn(msg, error);
    let message = error ? msg + ': ' + error : msg;
    this.warnObserver.next(message);
  }

  error(msg: string, error?: any) {
    console.error(msg, error);
    let message = error ? msg + ': ' + error : msg;
    this.errorObserver.next(message);
  }

  getInfos(): Observable<string> {
    return this.infoObservable;
  }

  getWarnings(): Observable<string> {
    return this.warnObservable;
  }

  getErrors(): Observable<string> {
    return this.errorObservable;
  }

}
