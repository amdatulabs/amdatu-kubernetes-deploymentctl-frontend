/**
 @license Licensed under Apache License v2. See LICENSE for more information.
 */
import { ErrorHandler } from '@angular/core';
import { Logger } from './logger';

export class MyExceptionHandler extends ErrorHandler {

  constructor(private _mylogger: Logger) {
    super(null);
  }

  call(error) {
    this._mylogger.error('Unhandled error!', error);
  }

}
