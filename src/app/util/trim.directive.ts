/**
 @license Licensed under Apache License v2. See LICENSE for more information.
 */
import { Directive, HostListener, ElementRef, Output, EventEmitter } from '@angular/core';

@Directive({
  selector: '[ngModel]][trim]'
})

export class Trim {

  @Output() ngModelChange: EventEmitter<any> = new EventEmitter();
  private el: any;

  constructor(el: ElementRef) {
    this.el = el.nativeElement;
  }

  @HostListener('keyup')
  onEvent() {
    let value: string = this.el.value;
    if (value && value.startsWith(' ') || value.endsWith(' ')) {
      this.el.value = value.trim();
      this.ngModelChange.emit(this.el.value);
    }
  }
}
