/**
 @license Licensed under Apache License v2. See LICENSE for more information.
 */
export class Host {

  static getBackendUrl(): string {
    return this.getBackendUrlInternal(window.location.protocol, window.location.host) + '/deploymentctl/rest';
  }

  static getBackendWebsocketUrl(): string {
    return this.getBackendWebsocketUrlInternal(window.location.protocol, window.location.host, true);
  }

  static isInDevEnv(): boolean {
    let host = window.location.host;
    return host.indexOf('localhost') >= 0 || host.indexOf('development') >=0;
  }

  private static getBackendUrlInternal(protocol: string, host: string): string {

    if (host.indexOf('localhost') >= 0) {
      // running in local dev server, connect to local dev backend
      return protocol + '//localhost:8080';
    } else {
      // running in docker compose or on k8s, backend is on same host and port as we are
      return protocol + '//' + host;
    }
  }

  private static getBackendWebsocketUrlInternal(protocol: string, host: string, useWsProtocol: boolean): string {

    let newProtocol = protocol;
    if (useWsProtocol) {
      if (protocol === 'http:') {
        newProtocol = 'ws:';
      } else if (protocol === 'https:') {
        newProtocol = 'wss:';
      }
    }
    let url = this.getBackendUrlInternal(newProtocol, host);
    return url + '/deploymentctl/websocket/';
  }

  public static getKeycloakUrl(): string {
    return Host.getKeycloakUrlInternal(window.location.protocol, window.location.host);
  }

  public static getKeycloakUrlInternal(protocol: string, host: string): string {
    if (host.indexOf('localhost') >= 0 || host.indexOf('development') >= 0) {
      return 'https://development-keycloak.cloud-rti.com';
    } else {
      let re = /(.*-)([\w]+)(\.cloud-rti\.com)/;
      let kc = host.replace(re, '$1keycloak$3');
      return protocol + '//' + kc;
    }
  }

}
