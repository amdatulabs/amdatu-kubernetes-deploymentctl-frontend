/**
 @license Licensed under Apache License v2. See LICENSE for more information.
 */
import { Component, Injectable, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';

@Component({
  selector: 'wait-dialog',
  template: `

    <div *ngIf="isShown" [config]="{ show: true, backdrop: 'static' }" (onHidden)="onHidden()" bsModal #autoShownModal="bs-modal"
         class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
            <h3 class="modal-title pull-left">Please wait...</h3>
          </div>
          <div class="modal-body">
            <h4>{{text}}</h4>
            <br>
            <br>
            <div style="width: 100%; text-align: center">
              <span style="font-size: 1.5em;" class="glyphicon glyphicon-refresh glyphicon-spin"></span>
            </div>
          </div>
        </div>
      </div>
    </div>

`

})

export class WaitDialogComponent {

  @ViewChild('autoShownModal') private autoShownModal:ModalDirective;

  private text: string;
  private isShown: boolean = false;
  private observer: Observer<boolean>;

  public showModal(text: string):Observable<boolean> {
    this.text = text;
    this.isShown = true;
    return new Observable(o => {
      this.observer = o;
    });
  }

  public appendText(text: string) {
    this.text += '\n\n' + text;
  }

  public hideModal():void {
    this.isShown = false;
  }

}

