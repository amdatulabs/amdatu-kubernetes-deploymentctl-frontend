/**
 @license Licensed under Apache License v2. See LICENSE for more information.
 */
import { Directive, forwardRef } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl } from '@angular/forms';

@Directive({
  selector: '[validateNoProtocol][formControlName],[validateNoProtocol][formControl],[validateNoProtocol][ngModel]',
  providers: [
    {provide: NG_VALIDATORS, useExisting: forwardRef(() => NoProtocolValidator), multi: true}
  ]
})
export class NoProtocolValidator implements Validator {
  constructor() {
  }

  validate(c: AbstractControl): { [key: string]: any } {
    let value = c.value;
    if (value && value.indexOf('://') >= 0) {
      return {validateNoProtocol: true};
    }
    return null;
  }
}
