/**
 @license Licensed under Apache License v2. See LICENSE for more information.
 */
import { Directive, forwardRef } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl } from '@angular/forms';

@Directive({
  selector: '[validatePort][formControlName],[validatePort][formControl],[validatePort][ngModel]',
  providers: [
    {provide: NG_VALIDATORS, useExisting: forwardRef(() => PortValidator), multi: true}
  ]
})
export class PortValidator implements Validator {
  constructor() {
  }

  validate(c: AbstractControl): { [key: string]: any } {
    let value = c.value;
    if (value && (isNaN(value) || value < 1 || value > 65535)) {
      return {validatePort: true};
    }
    return null;
  }
}
