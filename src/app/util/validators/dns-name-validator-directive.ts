/**
 @license Licensed under Apache License v2. See LICENSE for more information.
 */
import { Directive, forwardRef, Attribute } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl } from '@angular/forms';

@Directive({
  selector: '[validateDns][formControlName],[validateDns][formControl],[validateDns][ngModel]',
  providers: [
    {provide: NG_VALIDATORS, useExisting: forwardRef(() => DnsValidator), multi: true}
  ]
})
export class DnsValidator implements Validator {

  private static REGEX_DNS = new RegExp('^[a-z]([-a-z0-9]*[a-z0-9])?$');
  private static REGEX_DNS_HASH_LEADING_NUMBER = new RegExp('^(#|[a-z0-9]([-a-z0-9]*[a-z0-9])?)$');

  constructor(@Attribute('allowHashAndNumber') public allowHashAndNumber: string) {
  }

  validate(c: AbstractControl): { [key: string]: any } {
    let value = c.value;
    if (value && value.length > 0
      && this.allowHashAndNumber === 'true' ?
        !DnsValidator.REGEX_DNS_HASH_LEADING_NUMBER.test(value) : !DnsValidator.REGEX_DNS.test(value)) {
      return {validateDns: true};
    }
    return null;
  }
}
