/**
 @license Licensed under Apache License v2. See LICENSE for more information.
 */
import { Directive, forwardRef, Input } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl } from '@angular/forms';
import { SecretDataWrapper } from '../../secrets/models/secret';

@Directive({
  selector: '[validateSecretDataKey][formControlName],[validateSecretDataKey][formControl],' +
  '[validateSecretDataKey][ngModel]',
  providers: [
    {provide: NG_VALIDATORS, useExisting: forwardRef(() => SecretDataKeyValidator), multi: true}
  ]
})
export class SecretDataKeyValidator implements Validator {

  @Input('data') data: SecretDataWrapper[];

  constructor() {
  }

  validate(c: AbstractControl): { [key: string]: any } {
    if (!c.dirty) {
      return; // prevent validating readonly field
    }
    let value = c.value;
    if (!value) {
      return;
    }
    if (this.data.filter(wrapper => wrapper.key === value).length > 1) {
      return {validateSecretDataKey: true};
    }
    return null;
  }
}
