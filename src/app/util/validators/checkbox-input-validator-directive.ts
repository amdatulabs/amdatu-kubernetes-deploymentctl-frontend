/**
 @license Licensed under Apache License v2. See LICENSE for more information.
 */
import { Directive, forwardRef, Input } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl } from '@angular/forms';

@Directive({
  selector: '[validateCheckboxInput][formControlName],[validateCheckboxInput][formControl],' +
  '[validateCheckboxInput][ngModel]',
  providers: [
    {provide: NG_VALIDATORS, useExisting: forwardRef(() => CheckboxInputValidator), multi: true}
  ]
})
export class CheckboxInputValidator implements Validator {

  @Input('checkbox') public checkbox: string;
  @Input('input') public input: string;

  constructor() {
  }

  validate(c: AbstractControl): { [key: string]: any } {

    // if this the validator for the checkbox, just trigger validation of the input
    if (this.input) {
      c.root.get(this.input).updateValueAndValidity();
      return null;
    }

    // this is the input, do the actual validation
    let checkboxControl = c.root.get(this.checkbox);
    let inputControl = c;
    if (checkboxControl.value && !inputControl.value) {
      return {validateCheckboxInput: true};
    }
    return null;
  }
}
