/**
 @license Licensed under Apache License v2. See LICENSE for more information.
 */
import { Directive, forwardRef, Attribute } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl } from '@angular/forms';

@Directive({
  selector: '[validateCombinedLength][formControlName],[validateCombinedLength][formControl],' +
  '[validateCombinedLength][ngModel]',
  providers: [
    {provide: NG_VALIDATORS, useExisting: forwardRef(() => CombinedLengthValidator), multi: true}
  ]
})
export class CombinedLengthValidator implements Validator {
  constructor(@Attribute('otherInput') public otherInput: string,
              @Attribute('maxLength') public maxLength: number) {
  }

  validate(c: AbstractControl): { [key: string]: any } {

    // if this is the first input, just trigger validation of the other input
    if (!this.maxLength) {
      c.root.get(this.otherInput).updateValueAndValidity();
      return;
    }

    // this is the second input, do the validtion
    let length = 0;

    let other = c.root.get(this.otherInput).value;
    let me = c.value;

    if (other && other.length) {
      length += other.length;
    }
    if (me && me.length) {
      length += me.length;
    }

    if (length > this.maxLength) {
      return {validateCombinedLength: true};
    }
    return null;
  }
}
