/**
 @license Licensed under Apache License v2. See LICENSE for more information.
 */
import { Directive, forwardRef, Input } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl } from '@angular/forms';
import { NamespaceSecrets } from '../../secrets/models/secret';

@Directive({
  selector: '[validateSecretName][formControlName],[validateSecretName][formControl],[validateSecretName][ngModel]',
  providers: [
    {provide: NG_VALIDATORS, useExisting: forwardRef(() => SecretNameValidator), multi: true}
  ]
})
export class SecretNameValidator implements Validator {

  @Input('secrets') secrets: NamespaceSecrets;

  constructor() {
  }

  validate(c: AbstractControl): { [key: string]: any } {
    let value = c.value;
    if (!value) {
      return;
    }
    if (this.secrets && this.secrets.secrets &&
      this.secrets.secrets.filter(secret => secret.metadata.name === value).length > 1) {

      return {validateSecretName: true};

    }
    return null;
  }
}
