/**
 @license Licensed under Apache License v2. See LICENSE for more information.
 */
import { Directive, forwardRef } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl } from '@angular/forms';

@Directive({
  selector: '[validateCIdent][formControlName],[validateCIdent][formControl],[validateCIdent][ngModel]',
  providers: [
    {provide: NG_VALIDATORS, useExisting: forwardRef(() => CIdentValidator), multi: true}
  ]
})
export class CIdentValidator implements Validator {

  private static REGEX_C_IDENT = new RegExp('^[A-Za-z_][A-Za-z0-9_]*$');

  constructor() {
  }

  validate(c: AbstractControl): { [key: string]: any } {
    let value = c.value;
    if (value && value.length > 0 && !CIdentValidator.REGEX_C_IDENT.test(value)) {
      return {validateCIdent: true};
    }
    return null;
  }
}
