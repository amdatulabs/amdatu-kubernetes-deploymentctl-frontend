/**
 @license Licensed under Apache License v2. See LICENSE for more information.
 */
import { Component, Injectable, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';

@Component({
  selector: 'confirm-dialog',
  template: `

    <div *ngIf="isShown" [config]="{ show: true, backdrop: 'static' }" (onHidden)="onHidden()" bsModal #autoShownModal="bs-modal"
         class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
            <h3 class="modal-title pull-left">Confirm</h3>
          </div>
          <div class="modal-body">
            <h4>{{text}}</h4>
            <br>
            <div class="pull-right">
              <button class="btn btn-default" type="button" (click)="onHide(false)">Cancel</button>
              <button class="btn btn-info" type="button" (click)="onHide(true)">Ok</button>
            </div>
            <br>
            <br>
          </div>
        </div>
      </div>
    </div>

`

})

export class ConfirmDialogComponent {

  @ViewChild('autoShownModal') private autoShownModal:ModalDirective;

  private text: string;
  private isShown: boolean = false;
  private observer: Observer<boolean>;

  public showModal(text: string):Observable<boolean> {
    this.text = text;
    this.isShown = true;
    return new Observable(o => {
      this.observer = o;
    });
  }

  // public hideModal():void {
  //   this.autoShownModal.hide();
  // }

  private onHide(value: boolean):void {
    this.isShown = false;
    this.observer.next(value);
    this.observer.complete();
  }

}

