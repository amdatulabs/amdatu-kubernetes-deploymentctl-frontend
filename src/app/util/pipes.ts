/**
 @license Licensed under Apache License v2. See LICENSE for more information.
 */
import { Pipe, PipeTransform } from '@angular/core';
import { Base64 } from './base64';

@Pipe({name: 'keys', pure: true})
export class KeysPipe implements PipeTransform {
  transform(value: any, args: any[] = null): any {
    return value ? Object.keys(value) : [];
  }
}

@Pipe({name: 'base64Decode', pure: true})
export class Base64DecodePipe implements PipeTransform {
  transform(value: any, args: any[] = null): any {
    return Base64.decode(value);
  }
}
