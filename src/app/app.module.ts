/**
 @license Licensed under Apache License v2. See LICENSE for more information.
 */
import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpModule, Http } from '@angular/http';
import { RouterModule } from '@angular/router';
import { AuthHttp, AuthConfig } from 'angular2-jwt';
import { ModalModule, TypeaheadModule } from 'ngx-bootstrap';

import { DockerHubComponent } from './dockerHub/dockerHub.component';
import { ApplicationListComponent } from './deployment/application-list.component';
import { DeploymentDetailsComponent } from './deployment/deployment-details.component';
import { SecretListComponent } from './secrets/secret-list.component';
import { DeploymentService } from './deployment/services/deployment.service';
import { PullSecretService } from './deployment/services/pull-secret.service';
import { DockerHubService } from './dockerHub/services/dockerHub.service';
import { SecretService } from './secrets/services/secret.service';
import { Logger } from './util/logger';
import { KeycloakService } from './keycloak/keycloak.service';
import { SpinnerComponent } from './util/spinner.component';
import { Trim } from './util/trim.directive';
import { MyExceptionHandler } from './util/exceptionhandler';
import * as MyValidators from './util/validators/index';
import { BreadcrumbsComponent } from './breadcrumbs/breadcrumbs.component';


/*
 * Platform and Environment providers/directives/pipes
 */
import { ENV_PROVIDERS } from './environment';
import { ROUTES } from './app.routes';

// App is our top level component
import { AppComponent } from './app.component';
import { SecretComponent } from './secrets/secret.component';
import { NameSpaceComponent } from './secrets/namespace.component';
import { SecretDataComponent } from './secrets/secret-data.component';
import { DockerEnvComponent } from './deployment/docker-env.component';
import { DockerPortComponent } from './deployment/docker-port.component';
import { WebhookComponent } from './deployment/webhook.component';
import { DockerContainerComponent } from './deployment/docker-container.component';
import { HeaderComponent } from './deployment/header.component';
import { DescriptorDetailsComponent } from './deployment/descriptor-details.component';
import { KeysPipe, Base64DecodePipe } from './util/pipes';
import { DisableFormControlDirective } from './util/disable.directive';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ConfirmDialogComponent } from './util/confirm-dialog.component';
import { WaitDialogComponent } from './util/wait-dialog.component';
import { EventsService } from './util/events.service';

// Application wide providers
export const APP_PROVIDERS = [

  {
    provide: AuthHttp,
    useFactory: (http) => {
      return new AuthHttp(new AuthConfig({
        globalHeaders: [{'Content-Type': 'application/json'}],
        noJwtError: true,
        tokenGetter: () => {
          return window['_keycloak'].token;
        }
      }), http);
    },
    deps: [Http]
  },

  {
    provide: ErrorHandler,
    useFactory: (logger) => {
      return new MyExceptionHandler(logger);
    },
    deps: [Logger]
  },

  Logger, KeycloakService, EventsService,
  DockerHubService, SecretService, DeploymentService, PullSecretService

];

// application_directives: directives that are global through out the application
export const APP_DIRECTIVES = [
  /* global */
  SpinnerComponent, Trim, DisableFormControlDirective, ConfirmDialogComponent, WaitDialogComponent,
  /* validators */
  MyValidators.CIdentValidator, MyValidators.CheckboxInputValidator, MyValidators.CombinedLengthValidator,
  MyValidators.DnsValidator, MyValidators.PortValidator, MyValidators.SecretDataKeyValidator,
  MyValidators.SecretNameValidator, MyValidators.NoProtocolValidator,
  /* docker hub */
  DockerHubComponent,
  /* secrets */
  SecretListComponent, NameSpaceComponent, SecretComponent, SecretDataComponent,
  /* deployments */
  ApplicationListComponent, DeploymentDetailsComponent, DescriptorDetailsComponent, DockerContainerComponent,
  WebhookComponent, HeaderComponent, DockerPortComponent, DockerEnvComponent,
  /* Pipes */
  KeysPipe, Base64DecodePipe,
  /* breadcrumbs */
  BreadcrumbsComponent
];

/**
 * `AppModule` is the main entry point into Angular2's bootstraping process
 */
@NgModule({
  bootstrap: [AppComponent],
  declarations: [ // directives, components, and pipes owned by this NgModule
    AppComponent,
    APP_DIRECTIVES
  ],
  imports: [ // import Angular's modules
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(ROUTES, {useHash: true}),
    TypeaheadModule.forRoot(), ModalModule.forRoot(),
    SimpleNotificationsModule.forRoot()
  ],
  providers: [ // expose our Services and Providers into Angular's dependency injection
    ENV_PROVIDERS,
    APP_PROVIDERS
  ]
})
export class AppModule {
}
