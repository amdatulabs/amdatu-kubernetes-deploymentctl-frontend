/**
 @license Licensed under Apache License v2. See LICENSE for more information.
 */
import { Component, Input, OnInit, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { UUID } from 'angular2-uuid';
import { Logger } from '../util/logger';
import { Secret, NamespaceSecrets, SecretType, SecretDataWrapper, SECRET_DATA_DOCKER_KEY } from './models/secret';
import { SecretService } from './services/secret.service';

@Component({
  selector: 'secret',
  template: require('./views/secret.component.html')
})

export class SecretComponent implements OnInit {

  @Input() private namespaceSecrets: NamespaceSecrets;
  @Input() private secret: Secret;

  private dataWrappers: SecretDataWrapper[] = [];
  private isNew = false;
  private isDirty = false;
  private isDocker = false;

  private saveNotifier = new EventEmitter(false); // synchronous!

  private form: FormGroup;

  constructor(private _logger: Logger,
              private _secretService: SecretService,
              private _fb: FormBuilder) {
  }

  ngOnInit() {
    this.form = this._fb.group({
      'name': []
    });

    this.isNew = this.secret.metadata.name === '';
    this.isDocker = this.secret.type === SecretType.DOCKER_CONFIG.toString();
    this.initDataWrappers();

  }

  private initDataWrappers() {
    // remove data form controls
    for (let wrapper of this.dataWrappers) {
      this.form.removeControl(wrapper.id);
    }

    let keys = this.secret.data ? Object.keys(this.secret.data) : [];
    this.dataWrappers = [];
    for (let key of keys) {
      this.dataWrappers.push(this.createWrapper(key, this.secret.data[key], false));
    }
    if (this.isDocker && this.dataWrappers.length === 0) {
      this.dataWrappers.push(this.createWrapper(SECRET_DATA_DOCKER_KEY, '', true));
    }

  }

  private refreshSecretData() {
    this.secret.data = {};
    for (let wrapper of this.dataWrappers) {
      this.secret.data[wrapper.key] = wrapper.value;
    }
  }

  private createWrapper(key: string, value: string, isNew: boolean): SecretDataWrapper {
    let wrapper = <SecretDataWrapper>{};
    wrapper.id = UUID.UUID();
    wrapper.key = key;
    wrapper.value = value;
    wrapper.isNew = isNew;
    wrapper.isDocker = this.isDocker;
    return wrapper;
  }

  private addData() {
    this._logger.log('addData');
    this.dataWrappers.push(this.createWrapper('', '', true));
    this.isDirty = true;
  }

  private onDataDeleted(datawrapper: SecretDataWrapper) {
    this._logger.log('onDataDeleted');
    this.dataWrappers.splice(this.dataWrappers.indexOf(datawrapper), 1);
    if (datawrapper.isNew) {
      // if it was new unsaved data, we are not dirty anymore
      this.isDirty = false;
    } else {
      // else we are dirty now
      this.isDirty = true;
    }
  }

  private delete() {
    this._logger.log('deleting secret: ' + this.secret.metadata.name);
    this._secretService.deleteSecret(this.namespaceSecrets.namespace, this.secret.metadata.name).subscribe(
      _ => {
        this._logger.info('Secret deleted');
        this.namespaceSecrets.secrets.splice(this.namespaceSecrets.secrets.indexOf(this.secret), 1);
      },
      err => {
        this._logger.log('secret delete error: ' + err);
      },
      () => {
      }
    );
  }

  private save() {
    this.saveNotifier.emit('');
    this.refreshSecretData();
    this._logger.log('saving secret: ' + this.secret.metadata.name);
    this._secretService.setSecret(this.secret.metadata.namespace, this.secret).subscribe(
      secret => {
        this._logger.info('Secret saved');
        this.isNew = false;
        this.isDirty = false;
        this.renewLocalSecret(<Secret>secret);
      },
      err => {
        this._logger.error('save secret error', err);
      },
      () => {
      }
    );
  }

  private renewLocalSecret(remoteSecret: Secret) {
    // we need the new resource version, else next update will fail
    this.secret.metadata = remoteSecret.metadata;
    // just to be sure, update other properties, too
    this.secret.type = remoteSecret.type;
    this.secret.data = remoteSecret.data;
    this.initDataWrappers();
  }

}
