/**
 @license Licensed under Apache License v2. See LICENSE for more information.
 */
export const SECRET_API_VERSION = 'v1';
export const SECRET_KIND = 'Secret';

export interface NamespaceSecrets {
  namespace: string;
  secrets: Secret[];
  error: any;
}

export interface Secret {
  apiVersion: string;
  kind: string;
  metadata: {
    name: string;
    namespace: string;
    uid?: string;
    resourceVersion?: string;
  };
  data: {[key: string]: string};
  type: string;
}

export interface SecretDataWrapper {
  id: string;
  key: string;
  value: string;
  isNew: boolean;
  isDocker: boolean;
}

export class SecretType {
  // values
  static OPAQUE = new SecretType('Opaque');
  static DOCKER_CONFIG = new SecretType('kubernetes.io/dockercfg');

  static getAllTypes(): string[] {
    return [this.OPAQUE.toString(), this.DOCKER_CONFIG.toString()];
  }

  constructor(public value: string) {
  }

  toString() {
    return this.value;
  }

}

export interface DockerConfig {
  DOCKER_CONFIG_AUTH_KEY: DockerAuth;
}

export interface DockerAuth {
  email: string;
  auth: string;
}

export const SECRET_DATA_DOCKER_KEY = '.dockercfg';
export const DOCKER_CONFIG_AUTH_KEY = 'https://index.docker.io/v1/';
