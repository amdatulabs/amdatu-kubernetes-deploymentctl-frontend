/**
 @license Licensed under Apache License v2. See LICENSE for more information.
 */
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Base64 } from '../util/base64';
import { Logger } from '../util/logger';
import { DockerConfig, DOCKER_CONFIG_AUTH_KEY, SecretDataWrapper } from './models/secret';

@Component({
  selector: 'secret-data',
  template: require('./views/secret-data.component.html')
})

export class SecretDataComponent implements OnInit {

  @Input() private dataWrapper: SecretDataWrapper;
  @Input() private dataWrappers: SecretDataWrapper[];
  @Input() private parentControls: FormGroup;
  @Input() private saveNotifier: EventEmitter<any>;

  @Output() private deleteNotifier = new EventEmitter();

  private INPUT_TYPE_PLAINTEXT = 'Plain Text';
  private INPUT_TYPE_BASE64 = 'Base64 Encoded';

  private inputType: string = this.INPUT_TYPE_PLAINTEXT;
  private inputTypes: string[] = [this.INPUT_TYPE_PLAINTEXT, this.INPUT_TYPE_BASE64];

  private tmpValue = '';
  private tmpDockerEmail = '';
  private tmpDockerAuth = '';

  private form: FormGroup;

  constructor(private _logger: Logger,
              private _fb: FormBuilder) {
  }

  ngOnInit() {
    this.form = this._fb.group({
      inputType: [],
      key: [],
      value: [],
      mail: [],
      auth: []
    });

    this.init();

    let controlName = this.dataWrapper.id;
    if (!this.parentControls.controls[controlName]) {
      this._logger.log('adding data control ' + controlName);
      this.parentControls.addControl(controlName, this.form);
    }

    this.saveNotifier.subscribe(_ => this.save());

  }

  private init() {
    if (this.dataWrapper.isDocker) {
      if (this.dataWrapper.value) {
        let decoded = Base64.decode(this.dataWrapper.value);
        let dockerConfig = <DockerConfig>JSON.parse(decoded);
        this.tmpDockerEmail = dockerConfig[DOCKER_CONFIG_AUTH_KEY].email;
        let auth: string = dockerConfig[DOCKER_CONFIG_AUTH_KEY].auth;
        auth = auth.substr(0, 3) + '...';
        this.tmpDockerAuth = auth;
      } else {
        this.tmpDockerEmail = '';
        this.tmpDockerAuth = '';
      }
    } else {
      this.inputType = this.INPUT_TYPE_PLAINTEXT;
      this.tmpValue = this.dataWrapper.isNew ? '' : Base64.decode(this.dataWrapper.value);
    }

  }

  private onInputTypeChanged() {
    switch (this.inputType) {
      case this.INPUT_TYPE_PLAINTEXT:
        this.tmpValue = Base64.decode(this.tmpValue);
        break;
      case this.INPUT_TYPE_BASE64:
        this.tmpValue = Base64.encode(this.tmpValue);
        break;
      default:
        this._logger.error('unknown input type', null);
        return;
    }
  }

  private save() {
    this._logger.log('saving key ' + this.dataWrapper.key);
    if (this.dataWrapper.isDocker) {
      this.dataWrapper.value = this.getBase64DockerConfig();
    } else {
      switch (this.inputType) {
        case this.INPUT_TYPE_PLAINTEXT:
          this.dataWrapper.value = Base64.encode(this.tmpValue);
          break;
        case this.INPUT_TYPE_BASE64:
          this.dataWrapper.value = this.tmpValue;
          break;
        default:
          this._logger.error('unknown input type', null);
          return;
      }
    }
  }

  private delete() {
    this._logger.log('deleting data key: ' + this.dataWrapper.key);
    this.parentControls.removeControl(this.dataWrapper.id);
    this.deleteNotifier.emit('');
  }

  private getBase64DockerConfig() {
    let config = <DockerConfig>{};
    config[DOCKER_CONFIG_AUTH_KEY] = {auth: this.tmpDockerAuth, email: this.tmpDockerEmail};
    let json = JSON.stringify(config);
    let base64 = Base64.encode(json);
    return base64;
  }

}
