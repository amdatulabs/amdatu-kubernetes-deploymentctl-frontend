/**
 @license Licensed under Apache License v2. See LICENSE for more information.
 */
import { Component, OnInit } from '@angular/core';
import { NamespaceSecrets } from './models/secret';
import { SecretService } from './services/secret.service';
import { DockerHubService } from '../dockerHub/services/dockerHub.service';
import { Logger } from '../util/logger';

@Component({
  selector: 'secret-list',
  template: require('./views/secret-list.component.html')
})

export class SecretListComponent implements OnInit {

  private allNamespaceSecrets: NamespaceSecrets[] = [];

  private isLoggedIn = false;

  constructor(private _secretService: SecretService,
              private _dockerHubService: DockerHubService,
              private _logger: Logger) {
  }

  ngOnInit() {
    this._secretService.getAllNamespaceSecrets().subscribe(
      secrets => {
        if (!secrets.error) {
          this.allNamespaceSecrets.push(secrets);
          this.sortSecrets();
        }
      },
      err => this._logger.error('Error getting secrets', err),
      () => this._logger.log('getting secrets finished')
    );
  }

  private sortSecrets() {
    // sort namespace by name
    this.allNamespaceSecrets.sort((ns1, ns2) => ns1.namespace.localeCompare(ns2.namespace));
    this.allNamespaceSecrets.forEach(
      ns => {
        if (ns.secrets) {
          // sort secret by name
          ns.secrets.sort((s1, s2) => s1.metadata.name.localeCompare(s2.metadata.name));
        }
      }
    );
  }


}
