/**
 @license Licensed under Apache License v2. See LICENSE for more information.
 */
import { Component, Input } from '@angular/core';
import { NamespaceSecrets, SecretType, Secret } from './models/secret';
import { Logger } from '../util/logger';

@Component({
  selector: 'namespace',
  template: require('./views/namespace.component.html')
})

export class NameSpaceComponent {

  @Input() private namespaceSecrets: NamespaceSecrets;

  constructor(private _logger: Logger) {
  }

  private addOpaqueSecret() {
    this._logger.log('addOpaqueSecret');
    this.namespaceSecrets.secrets.push(this.createSecret(SecretType.OPAQUE));
  }

  private addImagePullSecret() {
    this._logger.log('addImagePullSecret');
    this.namespaceSecrets.secrets.push(this.createSecret(SecretType.DOCKER_CONFIG));
  }

  private createSecret(type: SecretType): Secret {
    let secret = <Secret>{};
    secret.metadata = {name: '', namespace: this.namespaceSecrets.namespace};
    secret.data = {};
    secret.type = type.toString();
    return secret;
  }

}
