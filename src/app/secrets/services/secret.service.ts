/**
 @license Licensed under Apache License v2. See LICENSE for more information.
 */
import { Injectable } from '@angular/core';
import { Headers } from '@angular/http';
import { AuthHttp } from 'angular2-jwt';
import { Observable } from 'rxjs/Rx';
import { NamespaceSecrets, Secret } from '../models/secret';
import { DockerHubService } from '../../dockerHub/services/dockerHub.service';
import { Logger } from '../../util/logger';
import { Host } from '../../util/host';


@Injectable()
export class SecretService {

  private host = Host.getBackendUrl();

  constructor(private _dockerHubService: DockerHubService,
              private _authHttp: AuthHttp,
              private _logger: Logger) {
  }

  getAllNamespaceSecrets(): Observable<NamespaceSecrets> {
    return this._dockerHubService.getNamespaces()
      .flatMap(namespace => this.getNamespaceSecrets(namespace));
  }

  getNamespaceSecrets(namespace: string): Observable<NamespaceSecrets> {

    return this._authHttp.get(
      this.host + '/secrets/' + namespace
    )
      .map(res => res.json())
      .map(json => this.mapToNamespaceSecrets(namespace, json))
      .catch(err => {
        if (err.toString().indexOf('JWT') >= 0) {
          // token expired
          this._logger.error('Session expired, please refresh.');
          return Observable.of(<NamespaceSecrets>{error: err});
        } else if (err.toString().indexOf('403') >= 0) {
          // forbidden
          this._logger.error('Access denied, session expired? Please refresh.');
          return Observable.of(<NamespaceSecrets>{error: err});
        } else {
          // something else happened...
          this._logger.error('Error getting secrets for namespace ' + namespace, err);
          return Observable.of(<NamespaceSecrets>{error: err});
        }
      });

  }

  setSecret(namespace: string, secret: Secret): Observable<Secret> {

    return this._authHttp.post(
      this.host + '/secrets/' + namespace,
      JSON.stringify(secret)
    )
      .map(res => res.json());
  }

  deleteSecret(namespace: string, secretName: string): Observable<any> {

    return this._authHttp.delete(
      this.host + '/secrets/' + namespace + '/' + secretName
    )
      .map(res => res.json());
  }

  private mapToNamespaceSecrets(namespace: string, json: any): NamespaceSecrets {
    if (!json) {
      json = [];
    }
    return <NamespaceSecrets>{'namespace': namespace, 'secrets': <Secret[]>json};
  }

}
