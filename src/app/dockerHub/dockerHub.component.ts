/**
 @license Licensed under Apache License v2. See LICENSE for more information.
 */
import { Component, OnInit } from '@angular/core';
import { Logger } from '../util/logger';
import { DockerHubService } from './services/dockerHub.service';
import { DockerUser } from './models/dockerUser';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'dockerHub',
  template: require('./views/dockerHub.component.html'),
})

export class DockerHubComponent implements OnInit {

  private dockerUser: DockerUser;
  private isDockerLoggedIn = false;
  private form: FormGroup;

  constructor(private _logger: Logger,
              private _router: Router,
              private _dockerHubService: DockerHubService,
              private _fb: FormBuilder) {
  }

  ngOnInit() {

    this.form = this._fb.group({
      name: [],
      password: []
    });

    this.isDockerLoggedIn = this._dockerHubService.dockerLoggedIn;
    if (this.isDockerLoggedIn) {
      this.dockerUser = {name: this._dockerHubService.dockerUsername, password: ''};
      this.updateForm();
    }

    this._dockerHubService.dockerLoggedIn$.subscribe(dockerLoggedIn => {
      this.isDockerLoggedIn = dockerLoggedIn;
      if (dockerLoggedIn) {
        this._logger.info("Docker Hub login successful");
        this.dockerUser = {name: this._dockerHubService.dockerUsername, password: ''};
        this._router.navigate(['listApplications']);
      } else {
        this._logger.info("Docker Hub logout successful");
        this.dockerUser = {name: '', password: ''};
      }
      this.updateForm();
    });

    this.form.valueChanges
      .filter(() => this.form.valid)
      .subscribe(v => this.updateModel(v));
  }

  private onDockerLogin() {
    this._dockerHubService.dockerLogin(this.dockerUser.name, this.dockerUser.password);
    this.dockerUser.password = '';
    this.updateForm();
  }

  private onDockerLogout() {
    this._dockerHubService.dockerLogout();
    this.dockerUser = {name: '', password: ''};
    this.updateForm();
  }

  private updateForm() {
    this.form.setValue(this.dockerUser);
  }

  private updateModel(dockerUser: DockerUser) {
    // this._logger.log('docker user: >' + dockerUser.name + '<');
    this.dockerUser = dockerUser;
  }
}
