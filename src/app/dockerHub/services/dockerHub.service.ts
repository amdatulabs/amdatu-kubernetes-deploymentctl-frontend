/**
 @license Licensed under Apache License v2. See LICENSE for more information.
 */
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { AuthHttp, JwtHelper } from 'angular2-jwt';
import { Repository, Tag } from '../models/repository';
import { Logger } from '../../util/logger';
import { Host } from '../../util/host';
import { SpinnerComponent } from '../../util/spinner.component';
import { Observer } from 'rxjs/Observer';

@Injectable()
export class DockerHubService {

  dockerLoggedIn$: Observable<boolean>;
  dockerLoggedIn = false;
  dockerUsername: string;

  private host = Host.getBackendUrl();

  private storageDockerUser = 'dockerUser';
  private storageDockerRepos = 'dockerRepos';

  private repositories: Repository[];

  private dockerLoginObserver: any;

  private jwtHelper: JwtHelper;

  constructor(private _http: Http,
              private _authHttp: AuthHttp,
              private _logger: Logger) {

    this.dockerLoggedIn$ = new Observable<boolean>(observer => {
      this.dockerLoginObserver = observer;
    }).share();
    this.dockerLoggedIn = false;
    this.dockerLoggedIn$.subscribe(dockerLoggedIn => {
      this.dockerLoggedIn = dockerLoggedIn;
      if (!dockerLoggedIn) {
        localStorage.removeItem(this.storageDockerUser);
        localStorage.removeItem(this.storageDockerRepos);
        this.dockerUsername = '';
        this.repositories = [];
        this._logger.log('docker logged out');
      }
    });

    this.jwtHelper = new JwtHelper();

    this.recoverDockerLogin();

  }

  getNamespaces(): Observable<string> {
    let decoded = this.jwtHelper.decodeToken(window['_keycloak'].token);
    return <Observable<string>>Observable.from(decoded['realm_access']['roles'])
      .filter(ns => ns !== 'admin');
  }

  dockerLogin(username: string, password: string) {

    this._authHttp.post(
      this.host + '/registry/login',
      '{"username": "' + username + '", "password": "' + password + '"}'
    )
      .map(res => res.json())
      .flatMap(
        _ => this.fetchRepositories()
      )
      .subscribe(
        () => {
        },
        err => this._logger.error('error during docker login', err),
        () => {
          localStorage.setItem(this.storageDockerUser, username);
          this.dockerUsername = username;
          this.dockerLoginObserver.next(true);
          this._logger.log('Docker login complete');
        }
      );
  }

  recoverDockerLogin() {

    this._authHttp.get(
      this.host + '/registry/username'
    )
      .map(res => res.text())
      .subscribe(
        username => this.dockerUsername = username,
        err => {
          if (err.toString().indexOf('Invalid JWT') >= 0) {
            this._logger.warn('Your Docker Hub token expired, please log in again if needed.');
            this._logger.log('docker token expired, could not recover docker login.');
          } else if (err.toString().indexOf('NO_DOCKER_TOKEN_FOUND') >= 0) {
            this._logger.log('no docker token found, could not recover docker login.');
          }
        },
        () => {
          if (this.dockerUsername) {
            this.fetchRepositories().subscribe(
              _ => {
              },
              err => this._logger.error('error during docker login', err),
              () => {
                localStorage.setItem(this.storageDockerUser, this.dockerUsername);
                this.dockerLoginObserver.next(true);
                this._logger.log('Docker recover login complete');
              }
            );
          }
        }
      );
  }

  fetchRepositories(): Observable<any> {
    SpinnerComponent.show('Loading images and tags from Docker Hub...');

    let observer: Observer<any>;
    let observable = new Observable(o => {
      observer = o;
    });

    this._logger.log('fetching repositories...');
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    this._authHttp.get(
      this.host + '/registry/repositories',
      {headers: headers}
    )
      .map(res => res.json())
      .flatMap(repositories => {
        this.repositories = repositories;
        return Observable.from(repositories);
      })
      .map(repository => this.enhanceRepository(<Repository>repository))
      .flatMap(repository => this.fetchTags(<Repository>repository))
      .timeout(10000) // sometimes request to docker hang
      .subscribe(
        _ => {
        },
        err => {
          this._logger.error('error during loading docker images and tags', err);
          SpinnerComponent.hide();
          observer.error(err);
        },
        () => {
          this._logger.log('fetch repositories and tags complete');
          localStorage.setItem(this.storageDockerRepos, JSON.stringify(this.repositories));
          SpinnerComponent.hide();
          observer.complete();
        }
      );

    return observable;
  }

  getRepositories() {
    return this.repositories;
  }

  dockerLogout() {
    this.dockerLoginObserver.next(false);
    this._authHttp.post(
      this.host + '/registry/logout', ''
    ).subscribe(
      () => {
      },
      e => this._logger.error('Docker Hub logout failed', e)
    );
  }

  private enhanceRepository(repository: Repository): Repository {
    repository.fullname = repository.namespace + '/' + repository.name;
    return repository;
  }

  private fetchTags(repository: Repository): Observable<any> {
    return this._authHttp.get(
      this.host + '/registry/' + repository.namespace + '/' + repository.name + '/tags'
    )
      .map(res => res.json())
      .flatMap(tags => {
        repository.tags = tags;
        return Observable.from(tags);
      })
      .map(tag => this.enhanceTag(<Tag>tag));
  }

  private enhanceTag(tag: Tag): Tag {
    tag.fullname = tag.name + ' (last updated: ' + tag.last_updated + ')';
    return tag;
  };

}
