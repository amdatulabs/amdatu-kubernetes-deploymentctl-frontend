/**
 @license Licensed under Apache License v2. See LICENSE for more information.
 */
export interface Repository {
  namespace: string;
  name: string;
  fullname?: string;
  tags?: Tag[];
}

export interface Tag {
  name: string;
  last_updated?: string;
  fullname?: string;
}
