/**
 @license Licensed under Apache License v2. See LICENSE for more information.
 */
export interface DockerUser {
  name: string;
  password?: string;
}
