/**
 @license Licensed under Apache License v2. See LICENSE for more information.
 */
/*
 * Angular bootstraping
 */
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { decorateModuleRef } from './app/environment';
/*
 * App Module
 * our top level module that holds all of our components
 */
import { AppModule } from './app';

import { enableProdMode } from '@angular/core';
import { Host } from "app/util/host";

declare var Keycloak: any;

/*
 * Bootstrap our Angular app with a top level NgModule
 */
export function main(): Promise<any> {

  let script = document.createElement('script');
  let host = Host.getKeycloakUrl();
  script.src = host + '/auth/js/keycloak.js';
  script.onload = function () {
    let keycloak = Keycloak('keycloak/keycloak.json');
    window['_keycloak'] = keycloak;

    keycloak.init(
      {onLoad: 'login-required'}
    )
      .success(function (authenticated) {

        if (!authenticated) {
          window.location.reload();
        }

        enableProdMode();

        // refresh login
        setInterval(function () {

          keycloak.updateToken(70).success(function (refreshed) {
            if (refreshed) {
              console.log('Token refreshed');
            } else {
              console.log('Token not refreshed, valid for ' +
                Math.round(keycloak.tokenParsed.exp + keycloak.timeSkew - new Date().getTime() / 1000) + ' seconds');
            }
          }).error(function () {
            console.error('Failed to refresh token');
          });

        }, 60000);

        console.log('Loading...');

        return platformBrowserDynamic()
          .bootstrapModule(AppModule)
          .then(decorateModuleRef)
          .catch(err => {
            console.log(err.message);
            console.error(err);
          } );
      });

  }

  document.head.appendChild(script);
  return Promise.resolve();
}

document.addEventListener('DOMContentLoaded', () => main());
