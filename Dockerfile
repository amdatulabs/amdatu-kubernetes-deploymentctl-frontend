FROM node:7-wheezy

RUN mkdir -p /www
COPY . /www
WORKDIR /www

RUN yarn global add rimraf
RUN npm run clean:install && npm run build:prod

CMD ["tail", "-f", "/dev/null"]
